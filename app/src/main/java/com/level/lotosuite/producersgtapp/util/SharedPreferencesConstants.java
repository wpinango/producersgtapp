package com.level.lotosuite.producersgtapp.util;

/**
 * Created by wpinango on 11/23/17.
 */

public class SharedPreferencesConstants {
    public static final String KEY_SESSION_TIME = "sesionTime";
    public static final String KEY_NOTIFICATION = "notification";
    public static final String KEY_NOTIFICATION_SOUND = "sound";
    public static final String KEY_GENERAL_NOTIFICATION = "notification";
    public static final String KEY_SHOW_RAFFE = "roulette";
    public static final String KEY_ROULETTE_SOUND = "rouletteSound";
    public static final String MUTE_ROULETTE = "muteSound";
    public static final String ID_SAVED = "user_id";
    public static final String KEY_TIME = "timeSynchro";
    private static final String KEY_DATA = "data";
}
