package com.level.lotosuite.producersgtapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.common.Format;
import com.level.lotosuite.producersgtapp.models.BalanceHistory;

import java.util.ArrayList;

/**
 * Created by wpinango on 11/1/17.
 */

public class BalanceListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<BalanceHistory> balances;
    private LayoutInflater inflater;

    public BalanceListAdapter(Context context, ArrayList<BalanceHistory> balances){
        this.context = context;
        this.balances = balances;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return balances.size();
    }

    @Override
    public Object getItem(int i) {
        return balances.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_balance, null);
        }
        BalanceHistory balance = this.balances.get(i);
        CardView cvComment = (CardView) view.findViewById(R.id.cv_balance);
        TextView tvTransactionType = (TextView)view.findViewById(R.id.tv_type_balance);
        TextView tvDate = (TextView)view.findViewById(R.id.tv_date_balance);
        TextView tvAmount = (TextView)view.findViewById(R.id.tv_amount_balance);
        TextView tvRef = (TextView)view.findViewById(R.id.tv_reference_balance);
        try {
            if (balance.getMode() == 1) {
                tvAmount.setText("Monto");
                tvDate.setText("Fecha");
                tvTransactionType.setText("Tipo");
                tvRef.setText("Referencia");
                cvComment.setBackgroundColor(Color.TRANSPARENT);
            } else {
                tvAmount.setText(Format.getCashFormat(balance.getAmount()));
                tvDate.setText(balance.getOperation_date());
                tvTransactionType.setText(balance.getType());
                tvRef.setText(balance.getReference().split("-")[0]);
                cvComment.setBackgroundColor(Color.WHITE);
            }
        }catch ( Exception e ) {
            e.getMessage();
        }
        return view;
    }
}
