package com.level.lotosuite.producersgtapp.interfaces;

/**
 * Created by wpinango on 10/31/17.
 */

public interface OnPlayerRequestApprove {
    String APPROVE = "APPROVE";

    void OnApproveButtonPressed(String buttonPressed, int id);
}
