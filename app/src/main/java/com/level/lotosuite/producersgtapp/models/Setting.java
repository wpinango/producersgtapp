package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 11/6/17.
 */

public class Setting {
    private String title;
    private boolean status;
    private String TAG;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getTAG() {
        return TAG;
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }
}
