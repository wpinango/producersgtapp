package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 8/7/17.
 */

public class Feature {
    private int id;
    private String name;
    private float value;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }


}
