package com.level.lotosuite.producersgtapp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/16/17.
 */

public class Player {
    private int id;
    private int playerid;
    private String name;
    private int active;
    private String user_name;
    private String phone;
    private String email;
    private String nick;
    private int amount;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getPlayerid() {
        return playerid;
    }

    public void setPlayerid(int playerid) {
        this.playerid = playerid;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public static boolean isItemSelected(ArrayList<Player> players) {
        for (Player p: players){
            if (p.isSelected()){
                return true;
            }
        }
        return false;
    }

    public static void changeSelectStatusItems(ArrayList<Player> players, boolean selected) {
        for (Player p: players){
            p.setSelected(selected);
        }
    }

    public static int getItemSelected(ArrayList<Player> players) {
        int count = 0;
        for (Player p: players){
            if (p.isSelected()){
                count++;
            }
        }
        return count;
    }

    public static ArrayList<Player> getItemsSelected(ArrayList<Player> players) {
        ArrayList<Player> temp = new ArrayList<>();
        for (Player p : players) {
            if (p.isSelected()){
                temp.add(p);
            }
        }
        return temp;
    }


}
