package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/19/17.
 */

public class RatingProducer {
    private String average;

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }
}
