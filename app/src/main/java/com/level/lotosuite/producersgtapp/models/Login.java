package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/17/17.
 */

public class Login {
    private String token;
    private String error;
    private int id;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
