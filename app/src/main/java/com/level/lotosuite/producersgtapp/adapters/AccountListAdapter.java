package com.level.lotosuite.producersgtapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.common.FourDigitBankAccountFormatWatcher;
import com.level.lotosuite.producersgtapp.models.BankAccount;
import com.level.lotosuite.producersgtapp.models.BankList;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/23/17.
 */

public class AccountListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private ArrayList<BankAccount> bankAccounts;
    private Context context;

    public AccountListAdapter(Context context, ArrayList<BankAccount> accountNumbers){
        this.inflater = LayoutInflater.from(context);
        this.bankAccounts = accountNumbers;
        this.context = context;
    }

    @Override
    public int getCount() {
        return bankAccounts.size();
    }

    @Override
    public Object getItem(int position) {
        return bankAccounts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_account, null);
        }
        BankAccount bankAccount = bankAccounts.get(position);
        CardView cvComment = (CardView) view.findViewById(R.id.cv_bank);
        TextView tvBankName = (TextView)view.findViewById(R.id.tv_name_producer);
        TextView tvAccountNumber = (TextView)view.findViewById(R.id.tv_comment_account_number);
        TextView tvType = (TextView)view.findViewById(R.id.tv_account_type);
        ImageView ivBank = (ImageView)view.findViewById(R.id.iv_bank);
        tvAccountNumber.addTextChangedListener(new FourDigitBankAccountFormatWatcher());
        try {
            tvAccountNumber.setText(bankAccount.getAccountnumber());
            tvBankName.setText(bankAccount.getBank());
            ivBank.setImageResource(BankList.getBankImages(context, bankAccount.getAccountnumber().substring(0,4)));
            tvType.setText(bankAccounts.get(position).getAccounttype());
            if (bankAccount.isSelected()) {
                cvComment.setCardBackgroundColor(Color.GRAY);
            } else {
                cvComment.setCardBackgroundColor(Color.WHITE);
            }
        }catch ( Exception e ) {
            e.getMessage();
        }
        return view;
    }
}
