package com.level.lotosuite.producersgtapp.common;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by wpinango on 11/22/17.
 */

public class Time {

    public static String getNotificationDate(String timeStamp) {
        long stamp = Long.valueOf(timeStamp);
        Date date = new Date(stamp);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yy hh:mm a");
        return sdf.format(date).toUpperCase();
    }
}
