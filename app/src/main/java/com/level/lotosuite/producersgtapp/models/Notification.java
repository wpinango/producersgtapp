package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 11/7/17.
 */

public class Notification {
    private String title;
    private String hour;
    private boolean selected;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
