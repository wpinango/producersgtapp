package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/31/17.
 */

public class NewPlayerRequest {
    private int playerId;
    private int playerStatus;

    public NewPlayerRequest(int playerId, int playerStatus) {
        this.playerId = playerId;
        this.playerStatus = playerStatus;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getPlayerStatus() {
        return playerStatus;
    }

    public void setPlayerStatus(int playerStatus) {
        this.playerStatus = playerStatus;
    }
}
