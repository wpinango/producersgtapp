package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/17/17.
 */

public class ResponseLogin {
    public long timestamp;
    public int status;
    public String error;
    public ProducerInformation message;
    public String usermame;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public ProducerInformation getMessage() {
        return message;
    }

    public void setMessage(ProducerInformation message) {
        this.message = message;
    }

    public String getUsermame() {
        return usermame;
    }

    public void setUsermame(String usermame) {
        this.usermame = usermame;
    }
}
