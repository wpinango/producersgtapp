package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/26/17.
 */

public class ResponseBanks {
    public long timestamp;
    public int status;
    public String error;
    public Bank[] message;
    public String usermame;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Bank[] getMessage() {
        return message;
    }

    public void setMessage(Bank[] message) {
        this.message = message;
    }

    public String getUsermame() {
        return usermame;
    }

    public void setUsermame(String usermame) {
        this.usermame = usermame;
    }
}
