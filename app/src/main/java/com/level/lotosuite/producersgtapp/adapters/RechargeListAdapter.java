package com.level.lotosuite.producersgtapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.common.Format;
import com.level.lotosuite.producersgtapp.common.FourDigitBankAccountFormatWatcher;
import com.level.lotosuite.producersgtapp.interfaces.OnTransactionItemButtonPressed;
import com.level.lotosuite.producersgtapp.models.PendingTransaction;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/19/17.
 */

public class RechargeListAdapter extends BaseAdapter {
    ArrayList<PendingTransaction> recharges;
    Context context;
    LayoutInflater inflater;
    OnTransactionItemButtonPressed listener;

    public RechargeListAdapter(Context context, ArrayList<PendingTransaction>recharges, OnTransactionItemButtonPressed listener){
        this.recharges = recharges;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return recharges.size();
    }

    @Override
    public Object getItem(int i) {
        return recharges.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_recharge, null);
        }
        PendingTransaction recharge = recharges.get(i);
        CardView cvComment = (CardView) view.findViewById(R.id.cv_recharge);
        TextView tvName = (TextView)view.findViewById(R.id.tv_name_recharge);
        TextView tvAmount = (TextView)view.findViewById(R.id.tv_amount_recharge);
        TextView tvDate = (TextView)view.findViewById(R.id.tv_date_recharge);
        TextView tvBank = (TextView)view.findViewById(R.id.tv_bank_recharge);
        TextView tvRef = (TextView)view.findViewById(R.id.tv_reference_recharge);
        TextView tvAccountNumber = (TextView)view.findViewById(R.id.tv_account_recharge);
        ImageButton btnApprove = (ImageButton)view.findViewById(R.id.btn_approve_recharge);
        ImageButton btnRefuse = (ImageButton)view.findViewById(R.id.btn_refuse_recharge);
        tvAccountNumber.addTextChangedListener(new FourDigitBankAccountFormatWatcher());
        try {
            tvName.setText(recharge.getPlayer_name());
            tvDate.setText(recharge.getOperation_date() + " " + recharge.getHour());
            tvAmount.setText(Format.getCashFormat(recharge.getAmount()));
            tvBank.setText(recharge.getBank());
            tvRef.setText(recharge.getRef_number());
            tvAccountNumber.setText(recharge.getAccount());
            if (recharge.isSelected()){
                cvComment.setCardBackgroundColor(Color.GRAY);
            } else {
                cvComment.setCardBackgroundColor(Color.WHITE);
            }
        }catch ( Exception e ) {
            e.getMessage();
        }
        btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onButtonPressed(OnTransactionItemButtonPressed.APPROVE, i);
            }
        });
        btnRefuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onButtonPressed(OnTransactionItemButtonPressed.REFUSE, i);
            }
        });
        return view;
    }
}
