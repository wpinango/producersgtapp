package com.level.lotosuite.producersgtapp.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.level.lotosuite.producersgtapp.GlobalVars;
import com.level.lotosuite.producersgtapp.MyAsyncTask;
import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.activities.AllPalyersActivity;
import com.level.lotosuite.producersgtapp.activities.MainActivity;
import com.level.lotosuite.producersgtapp.activities.ProducerInformationActivity;
import com.level.lotosuite.producersgtapp.adapters.NewPlayersListAdapter;
import com.level.lotosuite.producersgtapp.interfaces.OnPlayerRequestApprove;
import com.level.lotosuite.producersgtapp.interfaces.OnTaskCompleted;
import com.level.lotosuite.producersgtapp.models.NewPlayerRequest;
import com.level.lotosuite.producersgtapp.models.Player;
import com.level.lotosuite.producersgtapp.models.Response;
import com.level.lotosuite.producersgtapp.util.animation.TransitionAnimation;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/16/17.
 */

public class PrimaryFragment extends Fragment implements OnTaskCompleted, OnPlayerRequestApprove {
    private BootstrapButton tvInfo, tvAllUsers;
    private EditText etName;
    private ListView lvNewPlayers;
    private NewPlayersListAdapter newPlayersListAdapter;
    private ArrayList<Player>players = new ArrayList<>();
    private Toolbar toolbar;
    private TextView tvRating;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    public boolean isRefresh = false;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_users, null);
        etName = (EditText)rootView.findViewById(R.id.et_name);
        tvRating = (TextView) rootView.findViewById(R.id.tv_rating_p);
        mSwipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.sr_new_players);
        toolbar = (Toolbar)rootView.findViewById(R.id.toolbar4);
        toolbar.setVisibility(View.INVISIBLE);
        toolbar.inflateMenu(R.menu.menu_third_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_approve) {
                    if (Player.isItemSelected(players)) {
                        ArrayList<Player> temp = Player.getItemsSelected(players);
                        ArrayList<NewPlayerRequest> newPlayers = new ArrayList<>();
                        for (Player p : temp) {
                            newPlayers.add(new NewPlayerRequest(p.getId(),1));
                        }
                        confirmDialog(newPlayers);
                    } else {
                        Toast.makeText(getActivity(), "Debe seleccionar una peticion de usuario", Toast.LENGTH_SHORT).show();
                    }
                }
                if (item.getItemId() == R.id.menu_select) {
                    if (Player.isItemSelected(players) && Player.getItemSelected(players) != players.size()) {
                        selectAllItemsSelection();
                        ActionMenuItemView menu = (ActionMenuItemView) rootView.findViewById(R.id.menu_select);
                        menu.setText("Deseleccionar todo");
                    } else if (Player.getItemSelected(players) == players.size()) {
                        clearAllItemsSelection();
                        ActionMenuItemView menu = (ActionMenuItemView) rootView.findViewById(R.id.menu_select);
                        menu.setText("Seleccionar todo");
                    }
                }
                return false;
            }
        });
        lvNewPlayers = (ListView)rootView.findViewById(R.id.lv_new_players);
        newPlayersListAdapter = new NewPlayersListAdapter(getActivity(),players, PrimaryFragment.this);
        tvAllUsers = (BootstrapButton)rootView.findViewById(R.id.tv_all_users);
        tvInfo = (BootstrapButton)rootView.findViewById(R.id.tv_calification_detail);
        tvInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ProducerInformationActivity.class);
                startActivity(intent);
                TransitionAnimation.setInActivityTransition(getActivity());
            }
        });
        tvAllUsers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AllPalyersActivity.class);
                startActivity(intent);
                TransitionAnimation.setInActivityTransition(getActivity());
            }
        });
        try {
            populateView();
        } catch (Exception e) {
            e.getMessage();
        }
        lvNewPlayers.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                players.get(i).setSelected(!players.get(i).isSelected());
                if (Player.isItemSelected(players)) {
                    toolbar.setVisibility(View.VISIBLE);
                } else {
                    toolbar.setVisibility(View.INVISIBLE);
                }
                newPlayersListAdapter.notifyDataSetChanged();
                return false;
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (isRefresh) {
                populateView();
                isRefresh = false;
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void refreshContent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new MainActivity.RequetDataSynchronizationAsynctask(getActivity(), PrimaryFragment.this).execute(GlobalVars.URL_GET_DATA + "/" + GlobalVars.dataBase + "/" + GlobalVars.id);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 1000);

    }

    private void confirmDialog(ArrayList<NewPlayerRequest> players) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Importante");
        dialog.setMessage("¿Desea realizar esta(s) operacion(es)?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                JsonArray jsonArray = new JsonArray();
                for (NewPlayerRequest player : players) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("player_id",player.getPlayerId());
                    jsonObject.addProperty("player_status",player.getPlayerStatus());
                    jsonArray.add(jsonObject);
                }
                new MyAsyncTask.PostSendAsyncTaks(getActivity(), GlobalVars.URL_SEND_PLAYER_STATUS, jsonArray.toString(), PrimaryFragment.this).execute();
            }
        });
        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dialog.show();
    }

    @Override
    public void onTaskCompleted(String s) {
        try {
            if (s.equals("1")){
                populateView();
                ((MainActivity)getActivity()).populateTabCounterNotification();
            } else if (!s.equals("1") && !s.equals("Bad request")) {
                Response response = new Gson().fromJson(s,Response.class);
                if (response.getError().equals("")) {
                    GlobalVars.Toaster.get().showToast(getActivity(), response.getMessage(), Toast.LENGTH_SHORT);
                    new MainActivity.RequetDataSynchronizationAsynctask(getActivity(), this).execute(GlobalVars.URL_GET_DATA + "/" + GlobalVars.dataBase+ "/" + GlobalVars.id);
                }
            } else {
                GlobalVars.Toaster.get().showToast(getActivity(), "Algo ha salido mal", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            GlobalVars.Toaster.get().showToast(getActivity(), "Algo no esta bien", Toast.LENGTH_SHORT);
            e.printStackTrace();
        }

    }

    public void populateView(){
        players = GlobalVars.producerInformation.getPlayers();
        etName.setText(GlobalVars.producerInformation.getUser().get(0).getName());
        newPlayersListAdapter.notifyDataSetChanged();
        //tvRating.setText(Format.getFormattedStringOneDecimal(Float.valueOf(GlobalVars.producerInformation.getRating().get(0).getAverage())));
    }

    private void selectAllItemsSelection() {
        Player.changeSelectStatusItems(players, true);
        newPlayersListAdapter.notifyDataSetChanged();
    }

    public void clearAllItemsSelection() {
        Player.changeSelectStatusItems(players, false);
        if (Player.isItemSelected(players)) {
            toolbar.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.INVISIBLE);
        }
        newPlayersListAdapter.notifyDataSetChanged();
    }

    @Override
    public void OnApproveButtonPressed(String buttonPressed, int id) {
        ArrayList<NewPlayerRequest> newPlayerRequests = new ArrayList<>();
        newPlayerRequests.add(new NewPlayerRequest(id,1));
        if (buttonPressed.equals(OnPlayerRequestApprove.APPROVE)) {
            confirmDialog(newPlayerRequests);
        }
    }
}
