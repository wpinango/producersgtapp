package com.level.lotosuite.producersgtapp.util.services;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;
import com.level.lotosuite.producersgtapp.GlobalVars;
import com.level.lotosuite.producersgtapp.models.Response;

/**
 * Created by wpinango on 9/1/17.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        GlobalVars.tokenFB = token;
        new UpdateTokenFB().execute(GlobalVars.URL_UPDATE_FB_TOKEN);
    }

    private class UpdateTokenFB extends AsyncTask<String, Integer, String> {
        Gson gson = new Gson();

        @Override
        protected String doInBackground(String... params) {
            String currentUrl = params[0];
            String response;
            try {
                HttpRequest req = HttpRequest.post(currentUrl).accept("application/json")
                        .header(GlobalVars.KEY_TOKEN, GlobalVars.token)
                        .contentType("application/json")
                        .connectTimeout(5000);
                response = req.body();
            } catch (HttpRequest.HttpRequestException e) {
                response = "";
                GlobalVars.Toaster.get().showToast(MyFirebaseInstanceIDService.this, "Falla de red", Toast.LENGTH_SHORT);
            } catch (Exception e) {
                System.out.println("Error1 ex : " + e.getMessage());
                response = "";
            }
            return response;
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.getError().equals("")) {
                    //BooleanResponse booleanResponse = gson.fromJson(s,BooleanResponse.class);

                } else {
                    GlobalVars.Toaster.get().showToast(MyFirebaseInstanceIDService.this, response.getMessage(), Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
