package com.level.lotosuite.producersgtapp.interfaces;

/**
 * Created by wpinango on 10/28/17.
 */

public interface OnTransactionItemButtonPressed {
    String APPROVE = "APPROVE";
    String REFUSE = "REFUSE";

    void onButtonPressed(String buttonPressed, int position);
}
