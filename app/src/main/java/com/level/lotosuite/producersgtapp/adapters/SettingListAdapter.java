package com.level.lotosuite.producersgtapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.models.Setting;
import com.level.lotosuite.producersgtapp.util.notification.NotificationsConfig;

import java.util.ArrayList;

/**
 * Created by wpinango on 11/22/17.
 */

public class SettingListAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context context;
    private ArrayList<Setting> settings;
    private NotificationsConfig notificationsConfig = new NotificationsConfig();

    public SettingListAdapter(Context context, ArrayList<Setting> settings){
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.settings = settings;
    }

    @Override
    public int getCount() {
        return settings.size();
    }

    @Override
    public Object getItem(int position) {
        return settings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_setting, null);
        }
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title_setting);
        Switch s = (Switch) view.findViewById(R.id.switch1);
        try {
            tvTitle.setText(settings.get(position).getTitle());
            s.setChecked(settings.get(position).isStatus());
            s.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    settings.get(position).setStatus(b);
                    notifyDataSetChanged();
                    setNewNotificationConfig();
                }
            });
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }

    private void setNewNotificationConfig(){
        notificationsConfig.setNotificationConfig(context,new Gson().toJson(settings));
    }
}
