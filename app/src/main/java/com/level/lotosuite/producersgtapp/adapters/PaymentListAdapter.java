package com.level.lotosuite.producersgtapp.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.common.Format;
import com.level.lotosuite.producersgtapp.common.FourDigitBankAccountFormatWatcher;
import com.level.lotosuite.producersgtapp.interfaces.OnTransactionItemButtonPressed;
import com.level.lotosuite.producersgtapp.models.PendingTransaction;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/16/17.
 */

public class PaymentListAdapter extends BaseAdapter {
    ArrayList<PendingTransaction> payments;
    Context context;
    LayoutInflater inflater;
    OnTransactionItemButtonPressed listener;

    public PaymentListAdapter(Context context, ArrayList<PendingTransaction>payments, OnTransactionItemButtonPressed listener){
        this.payments = payments;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return payments.size();
    }

    @Override
    public Object getItem(int i) {
        return payments.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_payment, null);
        }
        PendingTransaction payment = payments.get(i);
        CardView cvComment = (CardView) view.findViewById(R.id.cardview);
        TextView tvName = (TextView)view.findViewById(R.id.tv_name_payment);
        TextView tvBank = (TextView)view.findViewById(R.id.tv_bank_payment);
        TextView tvDate = (TextView)view.findViewById(R.id.tv_date_payment);
        TextView tvRef = (TextView)view.findViewById(R.id.tv_ref_payment);
        TextView tvAmount = (TextView)view.findViewById(R.id.tv_amount_payment);
        TextView tvAccountNumber = (TextView)view.findViewById(R.id.tv_account_payment);
        ImageButton btnRefuse = (ImageButton)view.findViewById(R.id.btn_refuse_payment);
        ImageButton btnApprove = (ImageButton)view.findViewById(R.id.btn_approve_payment);
        tvAccountNumber.addTextChangedListener(new FourDigitBankAccountFormatWatcher());
        try {
            tvAmount.setText(Format.getCashFormat(payment.getAmount()));
            tvName.setText(payment.getPlayer_name());
            tvRef.setText(payment.getRef_number());
            tvDate.setText(payment.getOperation_date() +  " " + payment.getHour());
            if (payment.getType() == 3) {
                tvBank.setText("Cobro efectivo");
                tvAccountNumber.setText("");
            } else {
                tvBank.setText(payment.getBank());
                tvAccountNumber.setText(payment.getAccount());
            }
            if (payment.isSelected()){
                cvComment.setCardBackgroundColor(Color.GRAY);
            } else {
                cvComment.setCardBackgroundColor(Color.WHITE);
            }
        }catch ( Exception e ) {
            e.getMessage();
        }
        btnRefuse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onButtonPressed(OnTransactionItemButtonPressed.REFUSE, i);
            }
        });
        btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onButtonPressed(OnTransactionItemButtonPressed.APPROVE, i);
            }
        });
        return view;
    }


}
