package com.level.lotosuite.producersgtapp.models;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/23/17.
 */

public class BankList {

    public static int getBankImages(Context context, String bankCode){
        switch (bankCode) {
            case "0171":
                return context.getResources().getIdentifier("activo","drawable",context.getPackageName());
            case "0175":
                return context.getResources().getIdentifier("bicentenario","drawable",context.getPackageName());
            case "0128":
                return context.getResources().getIdentifier("caroni","drawable",context.getPackageName());
            case "0102":
                return context.getResources().getIdentifier("devenezuela","drawable",context.getPackageName());
            case "0114":
                return context.getResources().getIdentifier("bancaribe","drawable",context.getPackageName());
            case "0163":
                return context.getResources().getIdentifier("deltesoro","drawable",context.getPackageName());
            case "0115":
                return context.getResources().getIdentifier("exterior","drawable",context.getPackageName());
            case "0105":
                return context.getResources().getIdentifier("mercantil","drawable",context.getPackageName());
            case "0191":
                return context.getResources().getIdentifier("bnc","drawable",context.getPackageName());
            case "0108":
                return context.getResources().getIdentifier("provincial","drawable",context.getPackageName());
            case "0134":
                return context.getResources().getIdentifier("banesco","drawable",context.getPackageName());
            case "0174":
                return context.getResources().getIdentifier("banplus","drawable",context.getPackageName());
            case "0116":
                return context.getResources().getIdentifier("bod","drawable",context.getPackageName());
            case "0157":
                return context.getResources().getIdentifier("delsur","drawable",context.getPackageName());
            case "0151":
                return context.getResources().getIdentifier("bfc","drawable",context.getPackageName());
            case "0156":
                return context.getResources().getIdentifier("cienporciento","drawable",context.getPackageName());
            case "0172":
                return context.getResources().getIdentifier("bancamiga","drawable",context.getPackageName());
            case "0168":
                return context.getResources().getIdentifier("bancrecer","drawable",context.getPackageName());
            case "0177":
                return context.getResources().getIdentifier("banfanb","drawable",context.getPackageName());
            case "0146":
                return context.getResources().getIdentifier("bangente","drawable",context.getPackageName());
            case "0190":
                return context.getResources().getIdentifier("citibank","drawable",context.getPackageName());
            case "0169":
                return context.getResources().getIdentifier("mibanco","drawable",context.getPackageName());
            case "0138":
                return context.getResources().getIdentifier("plaza","drawable",context.getPackageName());
            case "0137":
                return context.getResources().getIdentifier("sofitasa","drawable",context.getPackageName());
        }
        return 0;
    }

    public static boolean isBankExist(Context context,String code){
        int bankImage = getBankImages(context,code);
        if (bankImage != 0){
            return true;
        }
        return false;
    }

    public static int findBankId(ArrayList<Bank>banks,String code){
        for (Bank bank:banks) {
            if (code.trim().equals(bank.getCode().trim())){
                return bank.getId();
            }
        }
        return 0;
    }

    public static String findBankName(ArrayList<Bank> banks, String code) {
        for (Bank bank: banks) {
            if (code.trim().equals(bank.getCode().trim())){
                return bank.getBank();
            }
        }
        return "";
    }
}
