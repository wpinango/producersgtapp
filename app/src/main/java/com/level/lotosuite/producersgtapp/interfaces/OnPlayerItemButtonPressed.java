package com.level.lotosuite.producersgtapp.interfaces;

/**
 * Created by wpinango on 10/28/17.
 */

public interface OnPlayerItemButtonPressed {
    String MESSAGE = "MESSAGE";
    String RECHARGE = "RECHARGE";
    String TRANSACTION = "TRANSACTION";

    void onButtonPressed(String buttonPressed, int id);
}
