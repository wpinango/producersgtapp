package com.level.lotosuite.producersgtapp.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.common.Format;
import com.level.lotosuite.producersgtapp.interfaces.OnPlayerRequestApprove;
import com.level.lotosuite.producersgtapp.models.Player;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/29/17.
 */

public class NewPlayersListAdapter extends BaseAdapter {
    ArrayList<Player> players;
    ArrayList<Player> playersFiltered;
    Context context;
    LayoutInflater inflater;
    OnPlayerRequestApprove listener;

    public NewPlayersListAdapter(Context context, ArrayList<Player>players, OnPlayerRequestApprove listener){
        this.players = players;
        this.playersFiltered = players;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return playersFiltered.size();
    }

    @Override
    public Object getItem(int i) {
        return playersFiltered.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_new_player, null);
        }
        Player player = playersFiltered.get(i);
        CardView cvComment = (CardView) view.findViewById(R.id.cv_player);
        TextView tvName = (TextView)view.findViewById(R.id.tv_new_name_player);
        TextView tvEmail = (TextView)view.findViewById(R.id.tv_new_email_layer);
        TextView tvUserNAme = (TextView)view.findViewById(R.id.tv_new_user_name_player);
        TextView tvPhone = (TextView)view.findViewById(R.id.tv_new_phone_player);
        TextView tvAmount = (TextView)view.findViewById(R.id.tv_amount);
        ImageButton btnApprove = (ImageButton)view.findViewById(R.id.btn_approve_new_player);
        try {
            tvName.setText(player.getName());
            tvEmail.setText(player.getEmail());
            tvPhone.setText(player.getPhone());
            tvUserNAme.setText(player.getNick());
            tvAmount.setText(Format.getCashFormat(player.getAmount()));
        }catch ( Exception e ) {
            e.getMessage();
        }
        btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.OnApproveButtonPressed(OnPlayerRequestApprove.APPROVE,player.getId());
            }
        });
        return view;
    }
}
