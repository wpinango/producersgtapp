package com.level.lotosuite.producersgtapp;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.level.lotosuite.producersgtapp.interfaces.OnTaskCompleted;
import com.github.kevinsawicki.http.HttpRequest;

/**
 * Created by wpinango on 10/16/17.
 */

public class MyAsyncTask {

    public static class PostRequestAsynctask extends AsyncTask {
        private Context context;
        private String url;
        private OnTaskCompleted listener;

        public PostRequestAsynctask(Context context, String url, OnTaskCompleted listener) {
            this.context = context;
            this.url = url;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            listener.onTaskCompleted(o.toString());
        }

    }

    public static class GetRequestAsynctask extends AsyncTask<String, String, String>  {
        private Context context;
        private String url;
        private OnTaskCompleted listener;
        private HttpRequest request;

        public GetRequestAsynctask(Context context, String url, OnTaskCompleted listener) {
            this.url = url;
            this.context = context;
            this.listener = listener;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.get(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .header(GlobalVars.KEY_ID, GlobalVars.id)
                        .header(GlobalVars.KEY_DB, GlobalVars.dataBase)
                        .accept("application/json")
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(context, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);
            listener.onTaskCompleted(o);
        }
    }

    public static class GetSendAsynTask extends AsyncTask {
        private Context context;
        private String url;
        private String jsonObject;
        private OnTaskCompleted listener;

        public GetSendAsynTask(Context context, String url, String jsonObject, OnTaskCompleted listener) {
            this.url = url;
            this.context = context;
            this.jsonObject = jsonObject;
            this.listener = listener;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            listener.onTaskCompleted(o.toString());
        }
    }

    public static class PostSendAsyncTaks extends AsyncTask<String, String, String> {
        private Context context;
        private String url;
        private String jsonObject;
        private OnTaskCompleted listener;
        private HttpRequest request;

        public PostSendAsyncTaks(Context context, String url, String jsonObject, OnTaskCompleted listener) {
            this.url = url;
            this.context = context;
            this.jsonObject = jsonObject;
            this.listener = listener;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.post(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .header(GlobalVars.KEY_ID, GlobalVars.id)
                        .header(GlobalVars.KEY_DB, GlobalVars.dataBase)
                        .accept("application/json")
                        .send(jsonObject)
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(context, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }


        @Override
        protected void onPostExecute(String o) {
            listener.onTaskCompleted(o);
        }
    }

    public static class Synchronization extends AsyncTask {
        private HttpRequest request;
        private Context context;
        private String url;

        public Synchronization(Context context, String url) {
            this.context = context;
            this.url = url;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            try {
                request = HttpRequest.get(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .accept("application/json")
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(context, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            System.out.println("valores + " + o);
        }
    }

}
