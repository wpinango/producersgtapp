package com.level.lotosuite.producersgtapp.activities;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.level.lotosuite.producersgtapp.GlobalVars;
import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.adapters.AccountListAdapter;
import com.level.lotosuite.producersgtapp.adapters.CommentsListAdapter;
import com.level.lotosuite.producersgtapp.common.Format;
import com.level.lotosuite.producersgtapp.common.FourDigitBankAccountFormatWatcher;
import com.level.lotosuite.producersgtapp.common.ListViewEnableScroll;
import com.level.lotosuite.producersgtapp.interfaces.OnTaskCompleted;
import com.level.lotosuite.producersgtapp.models.Bank;
import com.level.lotosuite.producersgtapp.models.BankAccount;
import com.level.lotosuite.producersgtapp.models.BankList;
import com.level.lotosuite.producersgtapp.models.Comment;
import com.level.lotosuite.producersgtapp.models.Feature;
import com.level.lotosuite.producersgtapp.models.ProducerInformation;
import com.level.lotosuite.producersgtapp.models.Response;
import com.level.lotosuite.producersgtapp.models.ResponseBanks;
import com.level.lotosuite.producersgtapp.models.ResponseComment;
import com.level.lotosuite.producersgtapp.util.animation.TransitionAnimation;
import com.level.lotosuite.producersgtapp.util.chart.Chart;
import com.github.kevinsawicki.http.HttpRequest;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by wpinango on 10/16/17.
 */

public class ProducerInformationActivity extends AppCompatActivity implements OnTaskCompleted {
    private ProgressBar progressBar;
    private android.support.v7.app.ActionBar actionBar;
    private EditText etName, etLocation, etEmail, etCed, etPhone;
    private TextView tvRating, tvTotalPlayers;
    private ListView lvBanks, lvComments;
    private HorizontalBarChart chart;
    private RatingBar ratingBar;
    private ArrayList<Comment> comments = new ArrayList<>();
    private CommentsListAdapter commentsListAdapter;
    private AccountListAdapter accountListAdapter;
    private ArrayList<BankAccount> bankAccounts = new ArrayList<>();
    private ArrayList<Bank> banks = new ArrayList<>();
    private FloatingActionButton btnAddAccount, btnDeleteAccount, btnEditAccount;
    private BankAccount bankAccount = new BankAccount();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producer_information);
        accountListAdapter = new AccountListAdapter(this, bankAccounts);
        tvTotalPlayers = (TextView) findViewById(R.id.tv_total_players);
        commentsListAdapter = new CommentsListAdapter(this, comments);
        btnAddAccount = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        btnDeleteAccount = (FloatingActionButton) findViewById(R.id.btn_delete_bank);
        btnEditAccount = (FloatingActionButton) findViewById(R.id.btn_edit_bank);
        hideFabButtons();
        etName = (EditText) findViewById(R.id.et_producer_name);
        etCed = (EditText) findViewById(R.id.et_ced);
        etPhone = (EditText)findViewById(R.id.et_producer_phone);
        etEmail = (EditText) findViewById(R.id.et_producer_email);
        etLocation = (EditText) findViewById(R.id.et_producer_location);
        tvRating = (TextView) findViewById(R.id.tv_producer_rating);
        ratingBar = (RatingBar) findViewById(R.id.rb_producer);
        progressBar = (ProgressBar) findViewById(R.id.progressBar4);
        progressBar.setVisibility(View.INVISIBLE);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Informacion y Calificacion");
        chart = (HorizontalBarChart) findViewById(R.id.chart);
        chart.setDoubleTapToZoomEnabled(false);
        lvBanks = (ListView) findViewById(R.id.lv_bank_account);
        lvBanks.setAdapter(accountListAdapter);
        ListViewEnableScroll.setListViewHeightBasedOnChildren(lvBanks);
        lvBanks.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        lvBanks.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                for (int j = 0; j < bankAccounts.size(); j++){
                    if (i == j) {
                        bankAccounts.get(i).setSelected(!bankAccounts.get(i).isSelected());
                    } else {
                        bankAccounts.get(j).setSelected(false);
                    }
                }
                accountListAdapter.notifyDataSetChanged();
                if (BankAccount.isAccountNumberSelected(bankAccounts)) {
                    btnDeleteAccount.setVisibility(View.VISIBLE);
                    btnEditAccount.setVisibility(View.VISIBLE);
                } else {
                    btnDeleteAccount.setVisibility(View.INVISIBLE);
                    btnEditAccount.setVisibility(View.INVISIBLE);
                }
                return false;
            }
        });
        lvComments = (ListView) findViewById(R.id.lv_comments);
        lvComments.setAdapter(commentsListAdapter);
        ListViewEnableScroll.setListViewHeightBasedOnChildren(lvComments);
        lvComments.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        btnAddAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBankAccount(true);
            }
        });
        btnDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BankAccount.isAccountNumberSelected(bankAccounts)) {
                    confirmDeleteDialog();
                } else {
                    Toast.makeText(ProducerInformationActivity.this,"Debe seleccionar un banco",Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnEditAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (BankAccount.isAccountNumberSelected(bankAccounts)) {
                    addBankAccount(false);
                } else {
                    Toast.makeText(ProducerInformationActivity.this,"Debe seleccionar un banco",Toast.LENGTH_SHORT).show();
                }
            }
        });
        new GetBanks().execute(GlobalVars.URL_GET_BANKS + "/" + GlobalVars.dataBase);
        new GetComments().execute(GlobalVars.URL_GET_COMMEMTS + GlobalVars.dataBase + "/" + GlobalVars.id);
        try {
            populateView();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            case R.id.action_refresh:
                progressBar.setVisibility(View.VISIBLE);
                new GetComments().execute(GlobalVars.URL_GET_COMMEMTS + GlobalVars.dataBase + "/" + GlobalVars.id);
                new MainActivity.RequetDataSynchronizationAsynctask(ProducerInformationActivity.this, this).execute(GlobalVars.URL_GET_DATA + "/" + GlobalVars.dataBase+ "/" + GlobalVars.id);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void populateView() {
        try {
            ProducerInformation producerInformation = GlobalVars.producerInformation;
            ArrayList<Feature> ratingProducer = producerInformation.getFeatures();
            tvTotalPlayers.setText(String.valueOf(producerInformation.getUser().get(0).getTotalplayers()));
            etName.setText(producerInformation.getUser().get(0).getName());
            etEmail.setText(producerInformation.getUser().get(0).getEmail());
            etLocation.setText(producerInformation.getUser().get(0).getCity());
            etCed.setText(producerInformation.getUser().get(0).getCedula());
            etPhone.setText(producerInformation.getUser().get(0).getPhone());
            bankAccounts.clear();
            bankAccounts.addAll(producerInformation.getBankAccounts());
            accountListAdapter.notifyDataSetChanged();
            Chart.paintChart(ratingProducer, Chart.setData(ratingProducer), chart);
            ratingBar.setRating(Float.valueOf(producerInformation.getRating().get(0).getAverage()));
            tvRating.setText(Format.getFormattedStringOneDecimal(Float.valueOf(producerInformation.getRating().get(0).getAverage())));
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void hideFabButtons(){
        btnEditAccount.setVisibility(View.INVISIBLE);
        btnDeleteAccount.setVisibility(View.INVISIBLE);
    }

    private void confirmDeleteDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(ProducerInformationActivity.this);
        dialog.setTitle("Importante");
        dialog.setMessage("¿Desea borrar esta cuenta?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new DeleteBankAccountTask().execute(GlobalVars.URL_DELETE_BANK_ACCOUNT +  "/" +String.valueOf(BankAccount.getAccountNumberSelected(bankAccounts).getAccountid()));
            }
        });
        dialog.setNegativeButton("Cancelar", null);
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    private void addBankAccount(boolean isAddAccount) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_add_bank_account, null);
        final RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radio_group_account);
        final EditText etAccountNumber = (EditText) view.findViewById(R.id.et_message_notification);
        etAccountNumber.addTextChangedListener(new FourDigitBankAccountFormatWatcher());
        if (isAddAccount) {
            builder.setTitle("Agregue numero de cuenta");
            builder.setPositiveButton("Agregar", (dialog, which) -> {
            });
        } else {
            BankAccount a = BankAccount.getAccountNumberSelected(bankAccounts);
            builder.setTitle("Modifique");
            etAccountNumber.setText(a.getAccountnumber());
            bankAccount.setBankId(a.getBankId());
            bankAccount.setBank(a.getAccountnumber());
            if (a.getAccounttype().equals("Corriente")) {
                radioGroup.check(R.id.rb_current);
            } else if (a.getAccounttype().equals("Ahorro")) {
                radioGroup.check(R.id.rb_savings);
            }
            builder.setPositiveButton("Editar", null);
        }
        builder.setCancelable(false);
        builder.setNegativeButton("Cerrar", null);
        builder.setView(view);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(view1 -> {
            InputMethodManager imm = (InputMethodManager) getSystemService(ProducerInformationActivity.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view1.getWindowToken(), 0);
            if (etAccountNumber.getText().length() != 0 && etAccountNumber.getText().length() == 24 &&
                    radioGroup.getCheckedRadioButtonId() != -1) {
                bankAccount.setAccountnumber(etAccountNumber.getText().toString());
                String code = bankAccount.getAccountnumber().substring(0, 4);
                int value = radioGroup.getCheckedRadioButtonId();
                if (value == R.id.rb_current) {
                    bankAccount.setAccounttype("Corriente");
                } else if (value == R.id.rb_savings) {
                    bankAccount.setAccounttype("Ahorro");
                }
                if (BankList.isBankExist(ProducerInformationActivity.this, code)) {
                    bankAccount.setBankId(BankList.findBankId(banks, code));
                    alertDialog.dismiss();
                    showBankFound(code, isAddAccount);
                }
            } else if (etAccountNumber.getText().length() != 24) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Ingrese un numero de cuenta valido", Toast.LENGTH_SHORT);
            } else if (radioGroup.getCheckedRadioButtonId() == -1) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Debe seleccionar un tipo de cuenta", Toast.LENGTH_SHORT);
            }
        });
    }

    private void showBankFound(String code, boolean isAddAccount) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_confirm_bank_account, null);
        TextView tvAccountBank = (TextView) view.findViewById(R.id.tv_bank_name_confirm);
        TextView tvTypeAccount = (TextView) view.findViewById(R.id.tv_type_account);
        TextView tvAccountNumber = (TextView) view.findViewById(R.id.tv_account_number_confirm);
        ImageView ivBank = (ImageView) view.findViewById(R.id.iv_bank_account);
        ivBank.setImageResource(BankList.getBankImages(ProducerInformationActivity.this, code));
        tvAccountBank.setText(BankList.findBankName(banks, code));
        tvTypeAccount.setText(bankAccount.getAccounttype());
        tvAccountNumber.setText((bankAccount.getAccountnumber()));
        builder.setTitle("Registro de cuenta");
        builder.setCancelable(false);
        builder.setPositiveButton("Confirmar", (dialog, which) -> {
            bankAccount.setAccountnumber(bankAccount.getAccountnumber().replace("-", ""));
            if (isAddAccount) {
                new RegisterBankAccountTask().execute(GlobalVars.URL_REGISTER_BANK_ACCOUNT);
            } else {
                new EditBankAccountTask().execute(GlobalVars.URL_EDIT_BANK_ACCOUNT + "/" + String.valueOf(BankAccount.getAccountNumberSelected(bankAccounts).getAccountid()));
            }
        });
        builder.setNegativeButton("Cancelar", (dialog, which) -> {

        });
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onTaskCompleted(String s) {
        try {
            if (s.equals("1")){
                progressBar.setVisibility(View.INVISIBLE);
                populateView();
            } else {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo ha salido mal", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo no esta bien", Toast.LENGTH_SHORT);
            e.printStackTrace();
        }
    }

    private class GetComments extends AsyncTask<String, Integer, String> {
        private HttpRequest request;
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            try {
                request = HttpRequest.get(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .accept("application/json")
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                comments.clear();
                ResponseComment response = gson.fromJson(s, ResponseComment.class);
                if (!s.isEmpty() && response.getError().equals("")) {
                    Collections.addAll(comments, response.getMessage());
                    commentsListAdapter.notifyDataSetChanged();
                } else {
                    GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo salio mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo ha salido mal", Toast.LENGTH_SHORT);
                e.printStackTrace();
            }
        }
    }

    private class GetBanks extends AsyncTask<String, Integer, String> {
        private HttpRequest request;
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            try {
                request = HttpRequest.get(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .accept("application/json")
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                ResponseBanks response = gson.fromJson(s, ResponseBanks.class);
                if (!s.isEmpty() && response.getError().equals("")) {
                    banks.addAll(Arrays.asList(response.getMessage()));
                    commentsListAdapter.notifyDataSetChanged();
                } else {
                    GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo salio mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo ha salido mal", Toast.LENGTH_SHORT);
                e.printStackTrace();
            }
        }
    }


    private class RegisterBankAccountTask extends AsyncTask<String, Integer, String> implements OnTaskCompleted {
        private HttpRequest request;
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("accountType", bankAccount.getAccounttype());
            jsonObject.addProperty("accountNumber", bankAccount.getAccountnumber());
            jsonObject.addProperty("bankId", bankAccount.getBankId());
            try {
                request = HttpRequest.get(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .header(GlobalVars.KEY_ID, GlobalVars.id)
                        .header(GlobalVars.KEY_DB, GlobalVars.dataBase)
                        .accept("application/json")
                        .send(jsonObject.toString())
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.getError().equals("")) {
                    GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, response.getMessage(), Toast.LENGTH_SHORT);
                    progressBar.setVisibility(View.VISIBLE);
                    new MainActivity.RequetDataSynchronizationAsynctask(ProducerInformationActivity.this, RegisterBankAccountTask.this).execute(GlobalVars.URL_GET_DATA + "/" + GlobalVars.dataBase+ "/" + GlobalVars.id);
                } else {
                    GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo ha salido mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo no esta bien", Toast.LENGTH_SHORT);
                e.printStackTrace();
            }
        }

        @Override
        public void onTaskCompleted(String s) {
            populateView();
            hideFabButtons();
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    private class EditBankAccountTask extends AsyncTask<String, Integer, String> implements OnTaskCompleted {
        private HttpRequest request;
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("accountType", bankAccount.getAccounttype());
            jsonObject.addProperty("accountNumber", bankAccount.getAccountnumber());
            try {
                request = HttpRequest.put(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .header(GlobalVars.KEY_ID, GlobalVars.id)
                        .header(GlobalVars.KEY_DB, GlobalVars.dataBase)
                        .accept("application/json")
                        .send(jsonObject.toString())
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.getError().equals("")) {
                    GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, response.getMessage(), Toast.LENGTH_SHORT);
                    progressBar.setVisibility(View.VISIBLE);
                    new MainActivity.RequetDataSynchronizationAsynctask(ProducerInformationActivity.this, EditBankAccountTask.this).execute(GlobalVars.URL_GET_DATA + "/" + GlobalVars.dataBase+ "/" + GlobalVars.id);
                } else {
                    GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo ha salido mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo no esta bien", Toast.LENGTH_SHORT);
                e.printStackTrace();
            }
        }

        @Override
        public void onTaskCompleted(String s) {
            populateView();
            hideFabButtons();
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

    private class DeleteBankAccountTask extends AsyncTask<String, Integer, String> implements OnTaskCompleted {
        private HttpRequest request;
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            try {
                request = HttpRequest.delete(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .header(GlobalVars.KEY_ID, GlobalVars.id)
                        .header(GlobalVars.KEY_DB, GlobalVars.dataBase)
                        .accept("application/json")
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                Response response = gson.fromJson(s, Response.class);
                if (response.getError().equals("")) {
                    GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, response.getMessage(), Toast.LENGTH_SHORT);
                    progressBar.setVisibility(View.VISIBLE);
                    new MainActivity.RequetDataSynchronizationAsynctask(ProducerInformationActivity.this, DeleteBankAccountTask.this).execute(GlobalVars.URL_GET_DATA + "/" + GlobalVars.dataBase+ "/" + GlobalVars.id);
                } else {
                    GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo ha salido mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                GlobalVars.Toaster.get().showToast(ProducerInformationActivity.this, "Algo no esta bien", Toast.LENGTH_SHORT);
                e.printStackTrace();
            }
        }

        @Override
        public void onTaskCompleted(String s) {
            populateView();
            hideFabButtons();
            progressBar.setVisibility(View.INVISIBLE);
        }
    }

}
