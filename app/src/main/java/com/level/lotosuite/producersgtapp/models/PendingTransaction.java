package com.level.lotosuite.producersgtapp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/16/17.
 */

public class PendingTransaction {
    private String player_name;
    private String operation_id;
    private int operation_status;
    private int amount;
    private String operation_date;
    private String ref_number;
    private int player_id;
    private String account;
    private String bank;
    private int type;
    private boolean selected;
    private String hour;

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getPlayer_name() {
        return player_name;
    }

    public void setPlayer_name(String player_name) {
        this.player_name = player_name;
    }

    public String getOperation_id() {
        return operation_id;
    }

    public void setOperation_id(String operation_id) {
        this.operation_id = operation_id;
    }

    public int getOperation_status() {
        return operation_status;
    }

    public void setOperation_status(int operation_status) {
        this.operation_status = operation_status;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getOperation_date() {
        return operation_date;
    }

    public void setOperation_date(String operation_date) {
        this.operation_date = operation_date;
    }

    public String getRef_number() {
        return ref_number;
    }

    public void setRef_number(String ref_number) {
        this.ref_number = ref_number;
    }

    public int getPlayer_id() {
        return player_id;
    }

    public void setPlayer_id(int player_id) {
        this.player_id = player_id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }


    public static boolean isItemSelected(ArrayList<PendingTransaction> recharges) {
        for (PendingTransaction p: recharges){
            if (p.isSelected()){
                return true;
            }
        }
        return false;
    }

    public static void changeSelectStatusItems(ArrayList<PendingTransaction> transactions, boolean selected) {
        for (PendingTransaction p: transactions){
            p.setSelected(selected);
        }
    }

    public static int getItemSelected(ArrayList<PendingTransaction> transactions) {
        int count = 0;
        for (PendingTransaction p: transactions){
            if (p.isSelected()){
                count++;
            }
        }
        return count;
    }

    public static ArrayList<PendingTransaction> getItemsSelected(ArrayList<PendingTransaction> transactions) {
        ArrayList<PendingTransaction> temp = new ArrayList<>();
        for (PendingTransaction p : transactions) {
            if (p.isSelected()){
                temp.add(p);
            }
        }
        return temp;
    }

}
