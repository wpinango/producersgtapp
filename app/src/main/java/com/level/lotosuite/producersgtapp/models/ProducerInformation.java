package com.level.lotosuite.producersgtapp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/19/17.
 */

public class ProducerInformation {
    private ArrayList<Producer>user;
    private ArrayList<RatingProducer>rating;
    private ArrayList<Feature> features;
    private ArrayList<PendingTransaction>refills;
    private ArrayList<PendingTransaction> payments;
    private ArrayList<Player>players;
    private ArrayList<BankAccount> bankAccounts;

    public ArrayList<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(ArrayList<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public ArrayList<Producer> getUser() {
        return user;
    }

    public void setUser(ArrayList<Producer> user) {
        this.user = user;
    }

    public ArrayList<RatingProducer> getRating() {
        return rating;
    }

    public void setRating(ArrayList<RatingProducer> rating) {
        this.rating = rating;
    }

    public ArrayList<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(ArrayList<Feature> features) {
        this.features = features;
    }

    public ArrayList<PendingTransaction> getRefills() {
        return refills;
    }

    public void setRefills(ArrayList<PendingTransaction> refills) {
        this.refills = refills;
    }

    public ArrayList<PendingTransaction> getPayments() {
        return payments;
    }

    public void setPayments(ArrayList<PendingTransaction> payments) {
        this.payments = payments;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }
}
