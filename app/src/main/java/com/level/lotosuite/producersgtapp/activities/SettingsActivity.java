package com.level.lotosuite.producersgtapp.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ListView;

import com.google.gson.Gson;
import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.adapters.SettingListAdapter;
import com.level.lotosuite.producersgtapp.models.Notification;
import com.level.lotosuite.producersgtapp.models.Setting;
import com.level.lotosuite.producersgtapp.util.SharedPreferencesConstants;
import com.level.lotosuite.producersgtapp.util.animation.TransitionAnimation;
import com.level.lotosuite.producersgtapp.util.notification.NotificationsConfig;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by wpinango on 11/6/17.
 */

public class SettingsActivity extends AppCompatActivity {
    private ArrayList<Setting> settings = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Ajustes");
        ListView listView = (ListView)findViewById(R.id.lv_setting);
        SettingListAdapter settingListAdapter = new SettingListAdapter(this, settings);
        listView.setAdapter(settingListAdapter);
        try {
            if (getSettings(this).isEmpty()) {
                setDefaultSettings(this);
                settings.addAll(getSettings(this));
            } else {
                settings.addAll(getSettings(this));
            }
            settingListAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public static ArrayList<Setting> getSettings(Context context){
        ArrayList<Setting> settings=  new ArrayList<>();
        Setting[] s = new Gson().fromJson(new NotificationsConfig().getNotification(context), Setting[].class);
        if (s != null) {
            settings.addAll(Arrays.asList(s));
        } else {
            settings.clear();
        }
        return settings;
    }

    public static ArrayList<Setting> setDefaultSettings(Context context) {
        ArrayList<Setting> settings = new ArrayList<>();
        Setting notification = new Setting();
        notification.setStatus(true);
        notification.setTitle("Notificacion");
        notification.setTAG(SharedPreferencesConstants.KEY_GENERAL_NOTIFICATION);
        Setting sound = new Setting();
        sound.setTitle("Sonido");
        sound.setStatus(true);
        sound.setTAG(SharedPreferencesConstants.KEY_NOTIFICATION_SOUND);
        settings.add(sound);
        settings.add(notification);
        new NotificationsConfig().setNotificationConfig(context, new Gson().toJson(settings));
        return settings;
    }
}
