package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/31/17.
 */

public class ResponseBalance {
    private long timestamp;
    private int status;
    private String error;
    private BalanceHistory[] message;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public BalanceHistory[] getMessage() {
        return message;
    }

    public void setMessage(BalanceHistory[] message) {
        this.message = message;
    }
}
