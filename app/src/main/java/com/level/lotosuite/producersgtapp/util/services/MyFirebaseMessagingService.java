package com.level.lotosuite.producersgtapp.util.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.level.lotosuite.producersgtapp.GlobalVars;
import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.activities.MainActivity;
import com.level.lotosuite.producersgtapp.activities.SettingsActivity;
import com.level.lotosuite.producersgtapp.common.LoginState;
import com.level.lotosuite.producersgtapp.common.Time;
import com.level.lotosuite.producersgtapp.models.FragmentPosition;
import com.level.lotosuite.producersgtapp.models.MessageType;
import com.level.lotosuite.producersgtapp.models.Notification;
import com.level.lotosuite.producersgtapp.models.Setting;
import com.level.lotosuite.producersgtapp.util.SharedPreferencesConstants;
import com.level.lotosuite.producersgtapp.util.notification.NotificationsConfig;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by wpinango on 8/31/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static String TAG = "MyFirebaseMsgService";
    private NotificationID notificationID = new NotificationID();
    private ArrayList<Setting> settings = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        try {
            Log.d(TAG, "From: " + remoteMessage.getFrom());

            if (remoteMessage.getData().size() > 0) {
                Log.d(TAG, "Message data payload: " + remoteMessage.getData());

                if (SettingsActivity.getSettings(this).isEmpty()) {
                    SettingsActivity.setDefaultSettings(this);
                } else {
                    settings.addAll(SettingsActivity.getSettings(this));
                }

                for (Setting setting : settings) {
                    switch (setting.getTAG()) {
                        case SharedPreferencesConstants.KEY_GENERAL_NOTIFICATION:
                            if (setting.isStatus() && !GlobalVars.isNotificationShow && LoginState.isLogin(this)) {
                                sendNotification(remoteMessage);
                            }
                            break;
                        case SharedPreferencesConstants.KEY_NOTIFICATION_SOUND:
                            if (setting.isStatus() && LoginState.isLogin(this)) {
                                makeANotificationSound();
                            }
                            break;
                    }
                }

                saveIncomingNotification(remoteMessage.getData());
                if (GlobalVars.isNotificationShow && LoginState.isLogin(this)) {
                    sendNotificationToFragmentsView(new Gson().toJson(remoteMessage.getData()));
                } else if (!GlobalVars.isNotificationShow && LoginState.isLogin(this)) {
                    checkNotificationMessageType(remoteMessage.getData());
                }
            }

            if (remoteMessage.getNotification() != null) {
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            }
        }catch (Exception e) {
            e.getMessage();
        }
    }

    private void saveIncomingNotification(Map<String,String >data ) {
        try {
            Notification notification = new Notification();
            if (NotificationsConfig.getSavedNotifications(this) != "") {
                ArrayList<Notification> a = new ArrayList<>(Arrays.asList(new Gson().fromJson(NotificationsConfig.getSavedNotifications(this), Notification[].class)));
                if (data.containsKey("title") && !data.get("title").equals("")) {
                    notification.setTitle(data.get("title"));
                    notification.setHour(Time.getNotificationDate(data.get("notificationDate")));
                    a.add(0, notification);
                }
                NotificationsConfig.saveNotifications(this, new Gson().toJson(a));
            } else {
                ArrayList<Notification> notifications = new ArrayList<>();
                if (data.containsKey("title") && !data.get("title").equals("")) {
                    notification.setTitle(data.get("title"));
                    notification.setHour(Time.getNotificationDate(data.get("notificationDate")));
                    notifications.add(notification);
                }
                NotificationsConfig.saveNotifications(this, new Gson().toJson(notifications));
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void checkNotificationMessageType(Map<String, String> data) {
        try {
            if (data.containsKey(MessageType.KEY_MESSAGE)) {
                String dataContains = data.get(MessageType.KEY_MESSAGE);
                if (dataContains.equals(MessageType.KEY_RECHARGE)) {
                    //TODO : actualizar todo los view de los fragments
                }
                updateFragmentViewFromNotification(data.get(MessageType.KEY_MESSAGE));
            }
        } catch (Exception e){
            e.getMessage();
        }
    }

    protected void sendNotificationToFragmentsView(String value) {
        Intent intent = new Intent("com.ruletadigital.ruletaplay.fragments.NotificationsFragment.action.UI_UPDATE");
        intent.putExtra("UI_KEY", value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    private void updateFragmentViewFromNotification(String value) {
        Intent intent = new Intent("com.ruletadigital.ruletaplay.fragments.Fragment.action.UI_UPDATE");
        intent.putExtra("UI_KEY2",value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * Schedule a job using FirebaseJobDispatcher.
     */
    /*private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }*/

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param remoteMessage FCM message body received.
     */
    private void sendNotification(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().containsKey("title") && !remoteMessage.getData().get("title").equals("")) {
            Intent intent = new Intent(this, MainActivity.class);
            if (remoteMessage.getData().containsKey(MessageType.KEY_MESSAGE)){
                String value = remoteMessage.getData().get(MessageType.KEY_MESSAGE);
                if (value.equals(MessageType.KEY_RECHARGE)) {
                    intent.putExtra(MessageType.KEY_MESSAGE, FragmentPosition.PrimaryFragment);
                }
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 , intent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.lotosuite)
                    .setContentText(remoteMessage.getData().get("title"))
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setAutoCancel(true)
                    //.setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(notificationID.getID(), notificationBuilder.build());
        }
    }

    private void makeANotificationSound() {
        try {
            Vibrator vb = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
            vb.vibrate(600);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class NotificationID {
        private final AtomicInteger c = new AtomicInteger(0);
        private int getID() {
            return c.incrementAndGet();
        }
    }
}

