package com.level.lotosuite.producersgtapp.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.level.lotosuite.producersgtapp.GlobalVars;
import com.level.lotosuite.producersgtapp.MyAsyncTask;
import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.activities.MainActivity;
import com.level.lotosuite.producersgtapp.adapters.PaymentListAdapter;
import com.level.lotosuite.producersgtapp.interfaces.OnTaskCompleted;
import com.level.lotosuite.producersgtapp.interfaces.OnTransactionItemButtonPressed;
import com.level.lotosuite.producersgtapp.models.PendingTransaction;
import com.level.lotosuite.producersgtapp.models.Response;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/16/17.
 */

public class PaymentFragment extends Fragment implements OnTaskCompleted, OnTransactionItemButtonPressed {
    private ListView lvPayment;
    private PaymentListAdapter paymentListAdapter;
    private ArrayList<PendingTransaction>payments = new ArrayList<>();
    private Toolbar toolbar;
    private ProgressBar progressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    public boolean isRefresh = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_payment, null);
        lvPayment = (ListView)rootView.findViewById(R.id.lv_payment);
        paymentListAdapter = new PaymentListAdapter(getActivity(),payments, this);
        lvPayment.setAdapter(paymentListAdapter);
        mSwipeRefreshLayout = (SwipeRefreshLayout)rootView.findViewById(R.id.sr_payment);
        progressBar = (ProgressBar)rootView.findViewById(R.id.progressBar3);
        progressBar.setVisibility(View.INVISIBLE);
        toolbar = (Toolbar)rootView.findViewById(R.id.toolbar3);
        toolbar.setVisibility(View.INVISIBLE);
        toolbar.inflateMenu(R.menu.menu_second_toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_approve) {
                    if (PendingTransaction.isItemSelected(payments)) {
                        ArrayList<PendingTransaction> temp = PendingTransaction.getItemsSelected(payments);
                        for (PendingTransaction p : temp) {
                            p.setOperation_status(1);
                        }
                        confirmApproveDialog(temp);
                    } else {
                        Toast.makeText(getActivity(),"Debe seleccionar un cobro",Toast.LENGTH_SHORT).show();
                    }
                }
                if (item.getItemId() == R.id.menu_refuse) {
                    if (PendingTransaction.isItemSelected(payments)) {
                        ArrayList<PendingTransaction> temp = PendingTransaction.getItemsSelected(payments);
                        for (PendingTransaction p : temp) {
                            p.setOperation_status(2);
                        }
                        confirmRefuseDialog(temp);
                    } else {
                        Toast.makeText(getActivity(),"Debe seleccionar un cobro",Toast.LENGTH_SHORT).show();
                    }
                }
                if (item.getItemId() == R.id.menu_select) {
                    if (PendingTransaction.isItemSelected(payments) && PendingTransaction.getItemSelected(payments) != payments.size()){
                        selectAllItemsSelection();
                        ActionMenuItemView menu = (ActionMenuItemView) rootView.findViewById(R.id.menu_select);
                        menu.setText("Deseleccionar todo");
                    } else if (PendingTransaction.getItemSelected(payments) == payments.size()) {
                        clearAllItemsSelection();
                        ActionMenuItemView menu = (ActionMenuItemView) rootView.findViewById(R.id.menu_select);
                        menu.setText("Seleccionar todo");
                    }
                }
                return false;
            }
        });
        lvPayment.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                payments.get(i).setSelected(!payments.get(i).isSelected());
                if (PendingTransaction.isItemSelected(payments)){
                    toolbar.setVisibility(View.VISIBLE);
                } else {
                    toolbar.setVisibility(View.INVISIBLE);
                }
                if (PendingTransaction.getItemSelected(payments) == payments.size()) {
                    ActionMenuItemView menu = (ActionMenuItemView) rootView.findViewById(R.id.menu_select);
                    menu.setText("Deseleccionar todo");
                }
                if (PendingTransaction.getItemSelected(payments) < payments.size()) {
                    ActionMenuItemView menu = (ActionMenuItemView) rootView.findViewById(R.id.menu_select);
                    menu.setText("Seleccionar todo");
                }
                paymentListAdapter.notifyDataSetChanged();
                return false;
            }
        });
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
        try {
            populateView();
        }catch ( Exception e) {
            e.getMessage();
        }
        return rootView;
    }

    public void populateView(){
        payments.clear();
        payments.addAll(GlobalVars.producerInformation.getPayments());
        paymentListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
        clearAllItemsSelection();
        if (isRefresh) {
            populateView();
            isRefresh = false;
        }
    }

    public void refreshContent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new MainActivity.RequetDataSynchronizationAsynctask(getActivity(), PaymentFragment.this).execute(GlobalVars.URL_GET_DATA + "/" + GlobalVars.dataBase + "/" + GlobalVars.id);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 1000);

    }

    public void clearAllItemsSelection(){
        PendingTransaction.changeSelectStatusItems(payments,false);
        if (PendingTransaction.isItemSelected(payments)){
            toolbar.setVisibility(View.VISIBLE);
        } else {
            toolbar.setVisibility(View.INVISIBLE);
        }
        paymentListAdapter.notifyDataSetChanged();
    }

    private void selectAllItemsSelection(){
        PendingTransaction.changeSelectStatusItems(payments,true);
        paymentListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onTaskCompleted(String s) {
        try {
            if (s.equals("")){
                progressBar.setVisibility(View.INVISIBLE);
                populateView();
                ((MainActivity)getActivity()).populateTabCounterNotification();
            } else if (!s.equals("")) {
                Response response = new Gson().fromJson(s,Response.class);
                if (response.getError().equals("")) {
                    GlobalVars.Toaster.get().showToast(getActivity(), response.getMessage(), Toast.LENGTH_SHORT);
                    progressBar.setVisibility(View.VISIBLE);
                    new MainActivity.RequetDataSynchronizationAsynctask(getActivity(), this).execute(GlobalVars.URL_GET_DATA + "/" + GlobalVars.dataBase+ "/" + GlobalVars.id);
                }
            } else {
                GlobalVars.Toaster.get().showToast(getActivity(), "Algo ha salido mal", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            GlobalVars.Toaster.get().showToast(getActivity(), "Algo no esta bien", Toast.LENGTH_SHORT);
            e.printStackTrace();
        }
    }

    @Override
    public void onButtonPressed(String buttonPressed, int position) {
        PendingTransaction payment = payments.get(position);
        ArrayList<PendingTransaction> r = new ArrayList<>();
        if (buttonPressed.equals(OnTransactionItemButtonPressed.APPROVE)) {
            payment.setOperation_status(1);
            r.add(payment);
            confirmApproveDialog(r);
        } else if (buttonPressed.equals(OnTransactionItemButtonPressed.REFUSE)) {
            payment.setOperation_status(2);
            r.add(payment);
            confirmRefuseDialog(r);
        }
    }

    private void confirmRefuseDialog(ArrayList<PendingTransaction> payments) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_msg, null);
        EditText etMessage = (EditText)view.findViewById(R.id.et_message_notification);
        builder.setTitle("Ingrese el motivo del rechazo de este(os) cobro(s) ");
        builder.setCancelable(false);
        builder.setPositiveButton("Enviar", (dialog, which) -> {
            if (!etMessage.getText().toString().equals("")){
                JsonArray jsonArray = new JsonArray();
                for (PendingTransaction payment : payments){
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("operation_id", payment.getOperation_id());
                    jsonObject.addProperty("operation_status", payment.getOperation_status());
                    jsonObject.addProperty("player_id", payment.getPlayer_id());
                    jsonObject.addProperty("amount", payment.getAmount());
                    jsonObject.addProperty("ref_number", payment.getRef_number());
                    jsonObject.addProperty("observations",etMessage.getText().toString());
                    jsonObject.addProperty("operation_type",payment.getType());
                    jsonArray.add(jsonObject);
                }
                new MyAsyncTask.PostSendAsyncTaks(getActivity(),GlobalVars.URL_DO_PAYMENT,jsonArray.toString(),PaymentFragment.this).execute();
            } else {
                Toast.makeText(getActivity(),"Debe ingresar el motivo de rechazo",Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Cancelar", (dialog, which) -> {

        });
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void confirmApproveDialog(ArrayList<PendingTransaction> payments) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Importante");
        dialog.setMessage("¿Desea aprobar este(os) cobro(s)?");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                JsonArray jsonArray = new JsonArray();
                for (PendingTransaction payment : payments) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("operation_id", payment.getOperation_id());
                    jsonObject.addProperty("operation_status", payment.getOperation_status());
                    jsonObject.addProperty("player_id", payment.getPlayer_id());
                    jsonObject.addProperty("amount", payment.getAmount());
                    jsonObject.addProperty("ref_number", payment.getRef_number());
                    jsonObject.addProperty("observations","");
                    jsonObject.addProperty("operation_type",payment.getType());
                    jsonArray.add(jsonObject);
                }
                new MyAsyncTask.PostSendAsyncTaks(getActivity(),GlobalVars.URL_DO_PAYMENT,jsonArray.toString(),PaymentFragment.this).execute();
            }
        });
        dialog.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        dialog.show();
    }
}
