package com.level.lotosuite.producersgtapp.activities;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.google.firebase.iid.FirebaseInstanceId;
import com.level.lotosuite.producersgtapp.GlobalVars;
import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.common.LoginState;
import com.level.lotosuite.producersgtapp.models.DataBase;
import com.level.lotosuite.producersgtapp.models.Login;
import com.level.lotosuite.producersgtapp.models.ResponseLogin;
import com.level.lotosuite.producersgtapp.sqlite.SQLite;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

/**
 * Created by wpinango on 10/17/17.
 */

public class LoginActivity extends AppCompatActivity {
    private TextView forgotLink;
    private EditText userLogin, passwordText;
    private BootstrapButton loginButton;
    private String status = "";
    private ProgressBar progressBar;
    private HttpRequest request;
    private Gson gson = new Gson();
    private boolean isTokenExist = false;
    private SQLite sqLite;
    private String userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        userLogin = (EditText) findViewById(R.id.input_user);
        forgotLink = (TextView) findViewById(R.id.tv_forgot_link);
        passwordText = (EditText) findViewById(R.id.input_pass);
        userLogin.setText("P8377875");
        passwordText.setText("8377875");
        //SharedPreferences sharedPreferences = getSharedPreferences("data", Context.MODE_PRIVATE);
        //userLogin.setText(sharedPreferences.getString("user", ""));
        GlobalVars.tokenFB = FirebaseInstanceId.getInstance().getToken();
        progressBar = (ProgressBar) findViewById(R.id.pb_login);
        progressBar.setVisibility(View.INVISIBLE);
        loginButton = (BootstrapButton) findViewById(R.id.btn_login);
        forgotLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                login();
            }
        });
        sqLite = new SQLite(this);
        Cursor cursor = sqLite.getUserData();
        if (cursor.moveToFirst()) {
            if (!cursor.getString(cursor.getColumnIndex("token")).equals(null)){
                isTokenExist = true;
            }
        }
    }

    public void login() {
        if (!validate()) {
            onLoginFailed();
            return;
        }
        loginButton.setEnabled(false);
        userName = userLogin.getText().toString();
        String password = passwordText.getText().toString();
        new LoginAsyncTask().execute(userName, password);
    }

    public boolean validate() {
        boolean valid = true;
        String user = userLogin.getText().toString();
        String password = passwordText.getText().toString();
        TextInputLayout textInputLayout = (TextInputLayout) findViewById(R.id.textInputLayout9);
        if (user.isEmpty()) {
            userLogin.setError("Introduzca un usuario valido");
            valid = false;
        } else {
            userLogin.setError(null);
        }
        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            textInputLayout.setError("Introduzca una contrasena valida");
            valid = false;
        } else {
            textInputLayout.setError(null);
        }
        return valid;
    }

    public void onLoginSuccess(String response) {
        LoginState.setLoginState(this, true);
        //Global.isLogin = true;
        progressBar.setVisibility(View.INVISIBLE);
        loginButton.setEnabled(true);
        if (isTokenExist) {
            sqLite.updateUserData(GlobalVars.token, userName, GlobalVars.id, GlobalVars.dataBase);
        } else {
            sqLite.insertUserData(GlobalVars.token,userName, GlobalVars.id, GlobalVars.dataBase);
        }
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra("response",response);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Fallo en Login", Toast.LENGTH_LONG).show();
        loginButton.setEnabled(true);
        progressBar.setVisibility(View.INVISIBLE);
    }

    private class LoginAsyncTask extends AsyncTask<String, Integer, String> {
        private HttpRequest request;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String user_name = params[0];
            String password = params[1];
            try {
                request = HttpRequest.post(GlobalVars.URL_LOGIN)
                        .accept("application/json")
                        .basic(user_name, password)
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(LoginActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            loginButton.setEnabled(true);
            progressBar.setVisibility(View.GONE);
            try {
                if (!s.isEmpty()) {
                    Login login = new Gson().fromJson(s,Login.class);
                    if (login.getError().equals("")) {
                        GlobalVars.token = login.getToken();
                        GlobalVars.id = login.getId();
                        new GetDBAsynctask().execute(GlobalVars.URL_GET_ALL_DB + "/"+GlobalVars.id);
                        GlobalVars.tokenFB = FirebaseInstanceId.getInstance().getToken();
                    } else {
                        onLoginFailed();
                        GlobalVars.Toaster.get().showToast(LoginActivity.this, "Algo no esta bien", Toast.LENGTH_SHORT);
                    }
                } else {
                    onLoginFailed();
                    GlobalVars.Toaster.get().showToast(LoginActivity.this, "Algo salio mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class GetDBAsynctask extends AsyncTask<String, Integer, String> {
        private HttpRequest request;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            try {
                request = HttpRequest.get(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .accept("application/json")
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(LoginActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            loginButton.setEnabled(true);
            progressBar.setVisibility(View.GONE);
            try {
                if (!s.isEmpty()) {
                    DataBase[] db = gson.fromJson(s,DataBase[].class);
                    if (!db[0].getDb().isEmpty()){
                        GlobalVars.dataBase = db[0].getDb();
                        new RequetDataAsynctask().execute(GlobalVars.URL_GET_DATA + "/" + GlobalVars.dataBase+ "/" + GlobalVars.id);
                        GlobalVars.tokenFB = FirebaseInstanceId.getInstance().getToken();
                    } else {
                        onLoginFailed();
                        GlobalVars.Toaster.get().showToast(LoginActivity.this, "Algo no esta bien", Toast.LENGTH_SHORT);
                    }
                } else {
                    onLoginFailed();
                    GlobalVars.Toaster.get().showToast(LoginActivity.this, "Algo salio mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private class RequetDataAsynctask extends AsyncTask<String, Integer, String> {
        private HttpRequest request;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            try {
                request = HttpRequest.get(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .header(GlobalVars.KEY_FB,GlobalVars.tokenFB)
                        .accept("application/json")
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(LoginActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            loginButton.setEnabled(true);
            progressBar.setVisibility(View.GONE);
            try {
                ResponseLogin responseLogin = gson.fromJson(s,ResponseLogin.class);
                if (!s.isEmpty() && responseLogin.getError().equals("")) {
                    GlobalVars.response = s;
                    GlobalVars.producerInformation = responseLogin.getMessage();
                    onLoginSuccess(s);
                } else {
                    onLoginFailed();

                    GlobalVars.Toaster.get().showToast(LoginActivity.this, "Algo salio mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                GlobalVars.Toaster.get().showToast(LoginActivity.this, "Algo salio mal", Toast.LENGTH_SHORT);
                e.printStackTrace();
            }
        }
    }
}
