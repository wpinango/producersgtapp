package com.level.lotosuite.producersgtapp.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.level.lotosuite.producersgtapp.GlobalVars;
import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.adapters.BalanceListAdapter;
import com.level.lotosuite.producersgtapp.models.ResponseBalance;
import com.level.lotosuite.producersgtapp.models.BalanceHistory;
import com.level.lotosuite.producersgtapp.util.animation.TransitionAnimation;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by wpinango on 10/31/17.
 */

public class BalanceActivity extends AppCompatActivity  {
    private android.support.v7.app.ActionBar actionBar;
    private ListView lvTransactions;
    private ArrayList<BalanceHistory> balances = new ArrayList<>();
    private int playerId;
    private BalanceListAdapter balanceListAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Usuarios");
        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.sr_balance);
        balanceListAdapter = new BalanceListAdapter(this,balances);
        lvTransactions = (ListView)findViewById(R.id.lv_transactions);
        lvTransactions.setAdapter(balanceListAdapter);
        playerId = getIntent().getIntExtra("playerId", 0);
        new RequestPlayerBalanceHistory(BalanceActivity.this, GlobalVars.URL_GET_BALANCE_HISTORY + "/" +GlobalVars.dataBase).execute();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            case R.id.menu_search:

                return true;
            case R.id.action_info:
                showInformationDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }

    private void showInformationDialog(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("RB: Recarga Bancaria");
        arrayList.add("RE: Recarga Efectivo");
        arrayList.add("SR: Saldo   Recibido");
        arrayList.add("ST: Saldo   Transferido");
        arrayList.add("CT: Cobro   Transferencia");
        arrayList.add("CE: Cobro   Efectivo");
        arrayList.add("TJ: Ticket  Jugado");
        arrayList.add("TP: Ticket  Premiado");
        arrayList.add("TA: Ticket  Anulado");
        arrayList.add("3P: Triple  Play");
        final AlertDialog.Builder builder = new AlertDialog.Builder(BalanceActivity.this);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(BalanceActivity.this, android.R.layout.simple_list_item_1, arrayList);
        builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setTitle("Leyenda");
        builder.setNegativeButton("Cerrar", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public void refreshContent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new RequestPlayerBalanceHistory(BalanceActivity.this, GlobalVars.URL_GET_BALANCE_HISTORY + "/" +GlobalVars.dataBase).execute();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 1000);

    }

    private class RequestPlayerBalanceHistory extends AsyncTask<String, String, String> {
        private Context context;
        private String url;
        private HttpRequest request;

        public RequestPlayerBalanceHistory(Context context, String url) {
            this.url = url;
            this.context = context;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.get(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .header("playerId", playerId)
                        .accept("application/json")
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(context, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String o) {
            try {
                balances.clear();
                if (!o.equals("")){
                    ResponseBalance response = new Gson().fromJson(o,ResponseBalance.class);
                    if (response.getError().equals("")){
                        balances.addAll(Arrays.asList(response.getMessage()));
                        balanceListAdapter.notifyDataSetChanged();
                    }
                } else {
                    Toast.makeText(BalanceActivity.this,"No se pudo cargar el balance",Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e){
                Toast.makeText(BalanceActivity.this,"Algo salio mal",Toast.LENGTH_SHORT).show();
                e.getMessage();
            }
        }
    }
}
