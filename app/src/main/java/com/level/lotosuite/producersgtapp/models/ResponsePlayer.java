package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/24/17.
 */

public class ResponsePlayer {
    public long timestamp;
    public int status;
    public String error;
    public Player[] message;
    public String usermame;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Player[] getMessage() {
        return message;
    }

    public void setMessage(Player[] message) {
        this.message = message;
    }

    public String getUsermame() {
        return usermame;
    }

    public void setUsermame(String usermame) {
        this.usermame = usermame;
    }
}
