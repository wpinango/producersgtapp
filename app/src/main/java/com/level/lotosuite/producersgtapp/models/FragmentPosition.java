package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/24/17.
 */

public class FragmentPosition {
    public static final int PrimaryFragment = 0;
    public static final int RechargeFragment = 1;
    public static final int PaymentFragment = 2;
    public static final int NotificationFragment = 3;
}
