package com.level.lotosuite.producersgtapp.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by wpinango on 11/1/17.
 */

public class SQLite  extends SQLiteOpenHelper{
    private static final String dataBaseName = "user.db";
    private static final int dataBaseVersion = 1;
    private final String tableName = "user_table";
    public static final String userName = "user_name";
    public static final String token = "token";
    public static final String userId = "user_id";
    public static String serverDataBaseName = "db_name";

    public SQLite(Context context) {
        super(context, dataBaseName, null, dataBaseVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table " + tableName +" (id integer primary key, " + userName +" text," +
                token + " text, "  + userId +" text, db_name text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists " + tableName);
        onCreate(sqLiteDatabase);
    }

    public boolean insertUserData(String token, String userName, int userId, String dataBaseName){
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.userName, userName);
        contentValues.put(this.token, token);
        contentValues.put(this.userId, userId);
        contentValues.put(this.serverDataBaseName, dataBaseName);
        db.insert(tableName,null,contentValues);
        db.close();
        return true;
    }

    public void updateUserData(String token, String userName, int userId, String dataBaseName) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(this.userName,userName);
        contentValues.put(this.userId,userId);
        contentValues.put(this.token,token);
        contentValues.put(this.serverDataBaseName, dataBaseName);
        db.update(tableName,contentValues,"id = ?", new String[] {String.valueOf(1)});
    }

    public Cursor getUserData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + tableName, null);
        return res;
    }
}
