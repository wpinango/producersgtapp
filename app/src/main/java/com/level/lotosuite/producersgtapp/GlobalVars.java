package com.level.lotosuite.producersgtapp;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.level.lotosuite.producersgtapp.models.Player;
import com.level.lotosuite.producersgtapp.models.ProducerInformation;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/16/17.
 */

public class GlobalVars {
    //private static final String HOST = "http://172.16.29.120/lot-up/public/index.php";
    private static final String HOST = "http://lotosuite.com:80";
    public static String URL_LOGIN = HOST + "/postLogin";
    public static String URL_GET_ALL_DB = HOST + "/getDBs";//TODO : pasar el id
    public static String URL_GET_DATA = HOST + "/getData";//TODO : pasar la db y id
    public static final String URL_GET_RECHARGE = HOST + "/getRefills/db/";//TODO : pasar el id
    public static final String URL_GET_PAYMENT = HOST +  "/getPayments/db/";//TODO : pasar el id
    public static final String URL_GET_PLAYERS = HOST + "/getPlayers/db/"; // TODO : pasar el id
    public static final String URL_SEND_RECHARGE = HOST + "/postRefills";
    public static final String URL_SEND_PAYMENT = HOST + "/postPayment";
    public static final String URL_SEND_PLAYER_STATUS = HOST + "/postPlayers";
    public static final String URL_GET_COMMEMTS = HOST + "/getComments/"; //TODO : pasar bd y id
    public static final String URL_GET_ALL_USERS = HOST + "/getPlayers/"; //TODO : pasar bd y id
    public static final String URL_REGISTER_BANK_ACCOUNT = HOST + "/postBankAccounts";
    public static final String URL_EDIT_BANK_ACCOUNT = HOST + "/putBankAccounts"; //TODO pasar el account id;
    public static final String URL_DELETE_BANK_ACCOUNT = HOST + "/deleteBankAccounts"; // TODO pasar el account id
    public static final String URL_SEND_DIRECT_MESSAGE_NOTIFICATION = HOST + "/postPersonalMessage";
    public static final String URL_DIRECT_RECHARGE = HOST + "/postDirectRecharge";
    public static final String URL_DO_RECHARGE = HOST + "/postRefills";
    public static final String URL_DO_PAYMENT = HOST + "/postPayment";
    public static final String URL_SEND_GENERAL_MESSAGE_NOTIFICATION = "/postMessage";
    public static final String URL_GET_BANKS = HOST + "/getBanks"; // TODO pasar el db;
    public static final String URL_GET_BALANCE_HISTORY = HOST + "/getPlayerBalance"; // TODO pasar la bd
    public static String token;
    public static String KEY_TOKEN = "bearer ";
    public static String KEY_AUTH = "Authorization";
    public static String KEY_ID = "id";
    public static String KEY_DB = "db";
    public static String KEY_FB = "x-msg-token";
    public static int id;
    public static String dataBase;
    public static String response;
    public static ProducerInformation producerInformation;
    public static ArrayList<Player> players = new ArrayList<>();
    public static int timeout = 10000;
    public static boolean isLogin = false;
    public static String tokenFB;
    public static String URL_UPDATE_FB_TOKEN;
    public static boolean isNotificationShow;
    public static boolean activityVisible;

    public enum Toaster {
        INSTANCE;
        private final Handler handler = new Handler(Looper.getMainLooper());

        public void showToast(final Context context, final String message, final int length) {
            if (context != null) {
                handler.post(
                        () -> Toast.makeText(context, message, length).show()
                );
            }
        }

        public static Toaster get() {
            return INSTANCE;
        }
    }
}
