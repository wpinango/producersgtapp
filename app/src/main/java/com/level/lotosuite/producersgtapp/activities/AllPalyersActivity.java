package com.level.lotosuite.producersgtapp.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.level.lotosuite.producersgtapp.GlobalVars;
import com.level.lotosuite.producersgtapp.MyAsyncTask;
import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.adapters.PlayersListAdapter;
import com.level.lotosuite.producersgtapp.interfaces.OnPlayerItemButtonPressed;
import com.level.lotosuite.producersgtapp.interfaces.OnTaskCompleted;
import com.level.lotosuite.producersgtapp.models.Player;
import com.level.lotosuite.producersgtapp.models.Response;
import com.level.lotosuite.producersgtapp.models.ResponsePlayer;
import com.level.lotosuite.producersgtapp.util.animation.TransitionAnimation;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by wpinango on 10/16/17.
 */


public class AllPalyersActivity extends AppCompatActivity implements OnTaskCompleted, OnPlayerItemButtonPressed {
    private android.support.v7.app.ActionBar actionBar;
    private ProgressBar progressBar;
    private ListView lvPlayers;
    private PlayersListAdapter playersListAdapter;
    private ArrayList<Player> players = new ArrayList<>();
    private String message;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private SearchView search;
    private MenuItem myActionMenuItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_user);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setSubtitle("Usuarios");
        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.sr_players);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);
        lvPlayers = (ListView)findViewById(R.id.lv_payers);
        playersListAdapter = new PlayersListAdapter(this,players, this);
        lvPlayers.setAdapter(playersListAdapter);
        if (players.isEmpty()){
            new GetAllPlayers().execute(GlobalVars.URL_GET_ALL_USERS + GlobalVars.dataBase + "/" + GlobalVars.id);
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search_users, menu);
        myActionMenuItem = menu.findItem( R.id.menu_search);
        SearchManager manager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        search = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        search.setQueryHint("Buscar por nombre...");
        search.setSearchableInfo(manager.getSearchableInfo(getComponentName()));
        search.setSubmitButtonEnabled(false);
        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String s) {
               /* if (myActionMenuItem != null) {
                    myActionMenuItem.collapseActionView();
                }
                search.setIconified(false);
                search.clearFocus();
                search.setQuery("", false);*/
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                playersListAdapter.getFilter().filter(s);
                return false;
            }

        });
        search.clearFocus();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            case R.id.menu_search:

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //invalidateOptionsMenu();
        /*if (myActionMenuItem != null) {
            myActionMenuItem.collapseActionView();
        }
        search.setIconified(false);
        search.clearFocus();
        search.setQuery("", false);*/
        TransitionAnimation.setOutActivityTransition(this);
    }

    public void refreshContent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                new GetAllPlayers().execute(GlobalVars.URL_GET_ALL_USERS + GlobalVars.dataBase + "/" + GlobalVars.id);
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }, 1000);

    }

    private void sendNotification(int id) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_msg, null);
        EditText etMessage = (EditText)view.findViewById(R.id.et_message_notification);
        builder.setTitle("Enviar notificacion");
        builder.setCancelable(false);
        builder.setPositiveButton("Enviar", (dialog, which) -> {
            if (!etMessage.getText().toString().equals("")) {
                message = etMessage.getText().toString();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("message", message);
                jsonObject.addProperty("playerId", id);
                new MyAsyncTask.PostSendAsyncTaks(this, GlobalVars.URL_SEND_DIRECT_MESSAGE_NOTIFICATION, jsonObject.toString(), AllPalyersActivity.this).execute();
            } else  {
                Toast.makeText(this,"Debe ingresar texto",Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Cancelar", (dialog, which) -> {

        });
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void sendRecharge(int id) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_cash_amount, null);
        EditText etAmount = (EditText)view.findViewById(R.id.et_cash_amount);
        builder.setTitle("Ingrese monto a recargar");
        builder.setCancelable(false);
        builder.setPositiveButton("Enviar", (dialog, which) -> {
            if (!etAmount.getText().toString().equals("") && !etAmount.getText().toString().equals("0") && Integer.valueOf(
                    etAmount.getText().toString()) >= 0) {
                int amount = Integer.valueOf(etAmount.getText().toString());
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("playerId", id);
                jsonObject.addProperty("amount", amount);
                new MyAsyncTask.PostSendAsyncTaks(this, GlobalVars.URL_DIRECT_RECHARGE, jsonObject.toString(), this).execute();
            } else {
                Toast.makeText(AllPalyersActivity.this,"Ingrese un valor valido", Toast.LENGTH_SHORT).show();
            }
        });
        builder.setNegativeButton("Cancelar", (dialog, which) -> {

        });
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onTaskCompleted(String s) {
        try {
            if (!s.equals("")) {
                Response response = new Gson().fromJson(s,Response.class);
                if (response.getError().equals("")){
                    Toast.makeText(AllPalyersActivity.this,response.getMessage(),Toast.LENGTH_SHORT).show();
                    new GetAllPlayers().execute(GlobalVars.URL_GET_ALL_USERS + GlobalVars.dataBase + "/" + GlobalVars.id);
                }
            } else {
                GlobalVars.Toaster.get().showToast(AllPalyersActivity.this,"Algo ha salido mal", Toast.LENGTH_SHORT);
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    public void onButtonPressed(String buttonPressed, int id) {
        if (buttonPressed .equals(OnPlayerItemButtonPressed.MESSAGE)) {
            sendNotification(id);
        } else if (buttonPressed.equals(OnPlayerItemButtonPressed.RECHARGE)){
            sendRecharge(id);
        } else if (buttonPressed.equals(OnPlayerItemButtonPressed.TRANSACTION)) {
            viewPlayerBalance(id);
        }
    }

    private void viewPlayerBalance(int playerId){
        Intent intent = new Intent(this,BalanceActivity.class);
        intent.putExtra("playerId",playerId);
        startActivity(intent);
        TransitionAnimation.setInActivityTransition(this);
    }

    private class GetAllPlayers extends AsyncTask<String, Integer, String> {
        private HttpRequest request;
        private Gson gson = new Gson();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            try {
                request = HttpRequest.get(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .accept("application/json")
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(AllPalyersActivity.this, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                progressBar.setVisibility(View.GONE);
                players.clear();
                ResponsePlayer response = gson.fromJson(s,ResponsePlayer.class);
                if (!s.isEmpty() && response.getError().equals("")) {
                    players.addAll(Arrays.asList(response.getMessage()));
                    playersListAdapter.notifyDataSetChanged();
                } else {
                    GlobalVars.Toaster.get().showToast(AllPalyersActivity.this, "Algo salio mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                GlobalVars.Toaster.get().showToast(AllPalyersActivity.this, "Algo salio mal", Toast.LENGTH_SHORT);
                e.printStackTrace();
            }
        }
    }
}
