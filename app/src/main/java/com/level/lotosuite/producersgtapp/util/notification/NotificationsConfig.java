package com.level.lotosuite.producersgtapp.util.notification;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * Created by wpinango on 9/5/17.
 */

public class NotificationsConfig {
    public static final String KEY_NOTIFICATION = "notification1";
    public static final String KEY_NOTIFICATION_TAG = "configNotification";

    public void setNotificationConfig(Context context,String value){
        SharedPreferences preferences = context.getSharedPreferences(KEY_NOTIFICATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString(KEY_NOTIFICATION_TAG, value);
        edit.apply();
    }

    public String getNotification(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_NOTIFICATION, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_NOTIFICATION_TAG,"");
    }

    public static void saveNotifications(Context context, String value) {
        SharedPreferences preferences = context.getSharedPreferences(KEY_NOTIFICATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putString("savedNotifications", value);
        edit.apply();
    }

    public static String getSavedNotifications(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_NOTIFICATION, Context.MODE_PRIVATE);
        return sharedPreferences.getString("savedNotifications", "");
    }

}
