package com.level.lotosuite.producersgtapp.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.level.lotosuite.producersgtapp.GlobalVars;
import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.adapters.SectionsPagerAdapter;
import com.level.lotosuite.producersgtapp.common.LoginState;
import com.level.lotosuite.producersgtapp.interfaces.OnTaskCompleted;
import com.level.lotosuite.producersgtapp.models.FragmentPosition;
import com.level.lotosuite.producersgtapp.models.ProducerInformation;
import com.level.lotosuite.producersgtapp.models.ResponseLogin;
import com.level.lotosuite.producersgtapp.sqlite.SQLite;
import com.level.lotosuite.producersgtapp.util.animation.TransitionAnimation;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.rahimlis.badgedtablayout.BadgedTabLayout;

public class MainActivity extends AppCompatActivity implements OnTaskCompleted {
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private BadgedTabLayout tabLayout;
    private ProgressBar progressBar;
    private final String ACTION_INTENT = "com.ruletadigital.ruletaplay.fragments.Fragment.action.UI_UPDATE";
    private final String ACTION_INTENT2 = "com.ruletadigital.ruletaplay.fragments.NotificationsFragment.action.UI_UPDATE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        progressBar = (ProgressBar)findViewById(R.id.progressBar5);
        progressBar.setVisibility(View.INVISIBLE);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(myOnPageChangeListener);
        tabLayout = (BadgedTabLayout)findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(mViewPager);
        FirebaseMessaging.getInstance().subscribeToTopic("news");
        GlobalVars.tokenFB = FirebaseInstanceId.getInstance().getToken();
        IntentFilter filter = new IntentFilter(ACTION_INTENT);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
        IntentFilter filterNotifications = new IntentFilter(ACTION_INTENT2);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiverNotification, filterNotifications);
    }

    private void populateVars(String response){
        ProducerInformation p = new Gson().fromJson(response,ProducerInformation.class);
        GlobalVars.producerInformation = p;
    }

    private String getBadgeCount(int value){
        if (value == 0) {
            return null;
        } else {
            return String.valueOf(value);
        }
    }

    public void populateTabCounterNotification(){
        tabLayout.setBadgeText(FragmentPosition.PrimaryFragment, getBadgeCount(GlobalVars.producerInformation.getPlayers().size()));
        tabLayout.setBadgeText(FragmentPosition.RechargeFragment, getBadgeCount(GlobalVars.producerInformation.getRefills().size()));
        tabLayout.setBadgeText(FragmentPosition.PaymentFragment, getBadgeCount(GlobalVars.producerInformation.getPayments().size()));
    }

    ViewPager.OnPageChangeListener myOnPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageScrollStateChanged(int state) {
            //Called when the scroll state changes.
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            //mSectionsPagerAdapter.rechargeClearSelection();
        }

        @Override
        public void onPageSelected(int position) {
            //mSectionsPagerAdapter.rechargeClearSelection();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        try {
            GlobalVars.activityVisible = true;
            if (GlobalVars.producerInformation == null) {
                SQLite sqLite = new SQLite(this);
                Cursor res = sqLite.getUserData();
                if (res.moveToFirst()) {
                    GlobalVars.token = res.getString(res.getColumnIndex(SQLite.token));
                    GlobalVars.id = res.getInt(res.getColumnIndex(SQLite.userId));
                    GlobalVars.dataBase = res.getString(res.getColumnIndex(SQLite.serverDataBaseName));
                }
                progressBar.setVisibility(View.VISIBLE);
                new RequetDataSynchronizationAsynctask(this, this).execute(GlobalVars.URL_GET_DATA + "/" + GlobalVars.dataBase + "/" + GlobalVars.id);
            }
            populateTabCounterNotification();
        } catch (Exception e) {
            e.getMessage();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        GlobalVars.activityVisible = false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,SettingsActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
            return true;
        }
        if (id == R.id.action_signout) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("Importante")
                    .setMessage("Desea cerrar la sesion?")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(intent);
                            MainActivity.this.finish();
                            TransitionAnimation.setInActivityTransition(MainActivity.this);
                            LoginState.setLoginState(MainActivity.this,false);
                        }
                    });
            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted(String s) {
        try {
            progressBar.setVisibility(View.INVISIBLE);
            if (s.equals("")) {
                populateTabCounterNotification();
                populateFragmentView();
            }
        } catch (Exception e) {
            e.getMessage();
        }
    }

    protected BroadcastReceiver receiverNotification = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT2.equals(intent.getAction())) {
                String value = intent.getStringExtra("UI_KEY");
                setNotificationReceive(value);
            }
        }
    };

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_INTENT.equals(intent.getAction()) && GlobalVars.activityVisible) {
                String value = intent.getStringExtra("UI_KEY2");
                updateCurrentFragmentFromNotification(value);
                //populateFragmentView();
            }
        }
    };

    private void setNotificationReceive(String value) {
        mSectionsPagerAdapter.updateNotificationReceive(value);
    }

    private void updateCurrentFragmentFromNotification(String value) {
        new RequetDataSynchronizationAsynctask(this, this).execute(GlobalVars.URL_GET_DATA + "/" + GlobalVars.dataBase + "/" + GlobalVars.id);
    }

    private void populateFragmentView(){
        mSectionsPagerAdapter.updateFragmentsViews();
        if (mViewPager.getCurrentItem() == FragmentPosition.PrimaryFragment) {
            mSectionsPagerAdapter.updatePrimaryFragmentView();
            mSectionsPagerAdapter.updateRechargeFragmentView();
        } else if (mViewPager.getCurrentItem() == FragmentPosition.RechargeFragment && mViewPager.getCurrentItem() == FragmentPosition.PaymentFragment ) {
            mSectionsPagerAdapter.updateRechargeFragmentView();
            mSectionsPagerAdapter.updatePaymentFragmentView();
        }
    }

    public static class RequetDataSynchronizationAsynctask extends AsyncTask<String, Integer, String> {
        private HttpRequest request;
        private Gson gson = new Gson();
        private Context context;
        private OnTaskCompleted listener;

        public RequetDataSynchronizationAsynctask(Context context, OnTaskCompleted listener){
            this.context = context;
            this.listener = listener;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            try {
                request = HttpRequest.get(url)
                        .header(GlobalVars.KEY_AUTH, GlobalVars.KEY_TOKEN + GlobalVars.token)
                        .header(GlobalVars.KEY_FB,GlobalVars.tokenFB)
                        .accept("application/json")
                        .connectTimeout(GlobalVars.timeout)
                        .readTimeout(GlobalVars.timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                GlobalVars.Toaster.get().showToast(context, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                ResponseLogin responseLogin = gson.fromJson(s,ResponseLogin.class);
                if (!s.isEmpty() && responseLogin.getError().equals("")) {
                    GlobalVars.response = s;
                    GlobalVars.producerInformation = responseLogin.getMessage();
                    if (listener != null) {
                        listener.onTaskCompleted("");
                    }
                } else {
                    listener.onTaskCompleted("Bad request");
                    GlobalVars.Toaster.get().showToast(context, "Algo ha salido mal", Toast.LENGTH_SHORT);
                }
            } catch (Exception e) {
                listener.onTaskCompleted("Bad request");
                GlobalVars.Toaster.get().showToast(context, "Algo salio mal", Toast.LENGTH_SHORT);
                e.printStackTrace();
            }
        }
    }

}
