package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/18/17.
 */

public class Producer {
    private String name;
    private int id;
    private String cedula;
    private String email;
    private String city;
    private int totalplayers;
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getTotalplayers() {
        return totalplayers;
    }

    public void setTotalplayers(int totalplayers) {
        this.totalplayers = totalplayers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
