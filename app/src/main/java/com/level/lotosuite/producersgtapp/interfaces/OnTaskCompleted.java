package com.level.lotosuite.producersgtapp.interfaces;

/**
 * Created by wpinango on 10/27/17.
 */

public interface OnTaskCompleted {
    void onTaskCompleted(String s);
}
