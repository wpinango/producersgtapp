package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/26/17.
 */

public class Bank {
    private String bank;
    private int id;
    private String code;

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
