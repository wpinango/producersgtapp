package com.level.lotosuite.producersgtapp.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.level.lotosuite.producersgtapp.fragments.NotificationFragment;
import com.level.lotosuite.producersgtapp.fragments.PrimaryFragment;
import com.level.lotosuite.producersgtapp.fragments.PaymentFragment;
import com.level.lotosuite.producersgtapp.fragments.RechargeFragment;

/**
 * Created by wpinango on 10/16/17.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {
    private PrimaryFragment primaryFragment = new PrimaryFragment();
    private RechargeFragment rechargeFragment = new RechargeFragment();
    private PaymentFragment paymentFragment = new PaymentFragment();
    private NotificationFragment notificationFragment = new NotificationFragment();

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return primaryFragment;
            case 1:
                return rechargeFragment;
            case 2:
                return paymentFragment;
            case 3:
                return notificationFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Informacion";
            case 1:
                return "Recargas";
            case 2:
                return "Pagos";
            case 3:
                return "Notificaciones";
        }
        return null;
    }

    public void updatePrimaryFragmentView(){
        primaryFragment.populateView();
    }

    public void updateRechargeFragmentView(){
        rechargeFragment.populateView();
    }

    public void updatePaymentFragmentView() {
        paymentFragment.populateView();
    }

    public void updateFragmentsViews() {
        primaryFragment.isRefresh = true;
        rechargeFragment.isRefresh = true;
        paymentFragment.isRefresh = true;
    }

    public void updateNotificationReceive(String value){
        notificationFragment.updateUIOnReceiverValue2(value);
    }

}

