package com.level.lotosuite.producersgtapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.models.Comment;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/23/17.
 */

public class CommentsListAdapter extends BaseAdapter {
    ArrayList<Comment> comments;
    Context context;
    LayoutInflater inflater;

    public CommentsListAdapter(Context context, ArrayList<Comment>comments){
        this.comments = comments;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return comments.size();
    }

    @Override
    public Object getItem(int i) {
        return comments.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_comment, null);
        }
        Comment comment = comments.get(i);
        TextView tvName = (TextView)view.findViewById(R.id.tv_date_balance);
        TextView tvComment = (TextView)view.findViewById(R.id.tv_comment);
        TextView tvDate = (TextView)view.findViewById(R.id.tv_comment_date);
        RatingBar rbRating = (RatingBar)view.findViewById(R.id.rb_producer_comment);
        try {
            tvName.setText(comment.getNick());
            tvComment.setText(comment.getComments());
            tvDate.setText(comment.getCreated_at().split(" ")[0]);
            rbRating.setRating(Float.valueOf(comment.getValue()));
        }catch ( Exception e ) {
            e.getMessage();
        }
        return view;
    }
}
