package com.level.lotosuite.producersgtapp.common;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by wpinango on 10/19/17.
 */

public class Format {

    public static String getFormattedStringOneDecimal(float value){
        return String.format("%.1f",value);
    }

    public static String getCashFormat(int cash) {
        String pattern = "#,###";
        DecimalFormat format = (DecimalFormat) NumberFormat.getNumberInstance(Locale.GERMAN);
        format.applyPattern(pattern);
        return format.format(cash);
    }
}
