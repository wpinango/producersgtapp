package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/31/17.
 */

public class BalanceHistory {
    private int amount;
    private int type;
    private String create_at;
    private String reference;
    private String operation_date;
    private int mode;

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getType() {
        return getTransactionTypeName(type);
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getOperation_date() {
        return operation_date;
    }

    public void setOperation_date(String operation_date) {
        this.operation_date = operation_date;
    }

    private String getTransactionTypeName(int transactionType) {
        String operationType = "";
        /*switch (transactionType) {
            case 1:
                return "Recarga Transferencia";
            case 2:
                return "Cobro Transferencia";
            case 3:
                return "Ticket Jugado";
            case 4:
                return "Ticket Premiado";
            case 5:
                return "Ticket Anulado";
            case 6:
                return "Saldo Recibido";
            case 7:
                return "Saldo Transferido";
            case 8:
                return "Recarga Efectivo";
            case 9:
                return "Cobro Efectivo";
            case 10:
                return "Triple Play";
        }
        return "";*/
        switch (transactionType) {
            case 1:
                operationType = "RB";
                break;
            case 2:
                operationType = "CT";
                break;
            case 3:
                operationType = "TJ";
                break;
            case 4:
                operationType = "TP";
                break;
            case 5:
                operationType = "TA";
                break;
            case 6:
                operationType = "SR";
                break;
            case 7:
                operationType = "ST";
                break;
            case 8:
                operationType = "RE";
                break;
            case 9:
                operationType = "CE";
                break;
            case 10:
                operationType = "3P";
                break;
        }
        return operationType;
    }
}
