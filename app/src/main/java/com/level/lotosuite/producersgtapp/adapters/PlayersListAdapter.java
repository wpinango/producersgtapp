package com.level.lotosuite.producersgtapp.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;

import com.level.lotosuite.producersgtapp.R;
import com.level.lotosuite.producersgtapp.common.Format;
import com.level.lotosuite.producersgtapp.interfaces.OnPlayerItemButtonPressed;
import com.level.lotosuite.producersgtapp.models.Player;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/19/17.
 */

public class PlayersListAdapter extends BaseAdapter implements Filterable {
    ArrayList<Player> players;
    ArrayList<Player> playersFiltered;
    Context context;
    LayoutInflater inflater;
    FriendFilter friendFilter;
    OnPlayerItemButtonPressed listener;

    public PlayersListAdapter(Context context, ArrayList<Player>players, OnPlayerItemButtonPressed listener){
        this.players = players;
        this.playersFiltered = players;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return playersFiltered.size();
    }

    @Override
    public Object getItem(int i) {
        return playersFiltered.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_player, null);
        }
        Player player = playersFiltered.get(i);
        CardView cvComment = (CardView) view.findViewById(R.id.cv_player);
        TextView tvName = (TextView)view.findViewById(R.id.tv_new_name_player);
        TextView tvEmail = (TextView)view.findViewById(R.id.tv_new_email_layer);
        TextView tvUserNAme = (TextView)view.findViewById(R.id.tv_new_user_name_player);
        TextView tvPhone = (TextView)view.findViewById(R.id.tv_new_phone_player);
        TextView tvAmount = (TextView)view.findViewById(R.id.tv_amount);
        ImageButton btnMessageNotification = (ImageButton)view.findViewById(R.id.btn_notification);
        ImageButton btnRecharge = (ImageButton)view.findViewById(R.id.btn_transfer);
        ImageButton btnTransaction = (ImageButton)view.findViewById(R.id.btn_transaction);
        try {
            tvName.setText(player.getName());
            tvEmail.setText(player.getEmail());
            tvPhone.setText(player.getPhone());
            tvUserNAme.setText(player.getNick());
            tvAmount.setText(Format.getCashFormat(player.getAmount()));
        }catch ( Exception e ) {
            e.getMessage();
        }
        btnMessageNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onButtonPressed(OnPlayerItemButtonPressed.MESSAGE, player.getPlayerid());
            }
        });
        btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onButtonPressed(OnPlayerItemButtonPressed.RECHARGE, player.getPlayerid());
            }
        });
        btnTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onButtonPressed(OnPlayerItemButtonPressed.TRANSACTION,player.getPlayerid());
            }
        });
        return view;
    }

    @Override
    public Filter getFilter() {
        if (friendFilter == null) {
            friendFilter = new FriendFilter();
        }

        return friendFilter;
    }

    private class FriendFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint!=null && constraint.length()>0) {
                ArrayList<Player> tempList = new ArrayList<Player>();

                // search content in friend list
                for (Player user : players) {
                    if (user.getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(user);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = players.size();
                filterResults.values = players;
            }

            return filterResults;
        }

        /**
         * Notify about filtered list to ui
         * @param constraint text
         * @param results filtered result
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            playersFiltered = (ArrayList<Player>) results.values;
            notifyDataSetChanged();
        }
    }
}
