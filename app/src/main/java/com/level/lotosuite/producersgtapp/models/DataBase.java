package com.level.lotosuite.producersgtapp.models;

/**
 * Created by wpinango on 10/18/17.
 */

public class DataBase {
    private String db;

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }
}
