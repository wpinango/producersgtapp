package com.level.lotosuite.producersgtapp.models;

import java.util.ArrayList;

/**
 * Created by wpinango on 10/23/17.
 */

public class BankAccount {
    private String accounttype;
    private String accountnumber;
    private String bank;
    private int bankId;
    private boolean selected;
    private int accountid;

    public int getAccountid() {
        return accountid;
    }

    public void setAccountid(int accountid) {
        this.accountid = accountid;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getBankId() {
        return bankId;
    }

    public void setBankId(int bankId) {
        this.bankId = bankId;
    }

    public String getAccounttype() {
        return accounttype;
    }

    public void setAccounttype(String accounttype) {
        this.accounttype = accounttype;
    }

    public String getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(String accountnumber) {
        this.accountnumber = accountnumber;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public static boolean isAccountNumberSelected(ArrayList<BankAccount> bankAccounts){
        for (BankAccount a : bankAccounts ) {
            if (a.isSelected()){
                return true;
            }
        }
        return false;
    }

    public static BankAccount getAccountNumberSelected(ArrayList<BankAccount> bankAccounts){
        for (BankAccount a: bankAccounts) {
            if (a.isSelected()){
                return a;
            }
        }
        return null;
    }

}
